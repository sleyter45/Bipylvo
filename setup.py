#!/usr/bin/env python3
"""Archivo "setup" correspondiente al proyecto Bipylvo.

El archivo comprende la función de instalación del proyecto y la instalación de
dependencias del sistema.

Basado en:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""
# -----------------------------------Libs--------------------------------------
import sys

from codecs import open

from os import path

from bipylvo import __version__

from setuptools import find_packages, setup
# -----------------------------------------------------------------------------
# Obtener la descripción larga del archivo a partir del README.rst
aqui = path.abspath(path.dirname(__file__))
try:
    with open(path.join(aqui, 'README.md'), encoding='utf-8') as f:
        descripcionLarga = f.read()
except Exception as err:
    texto = 'No se puede acceder al README.md'
    sys.stderr.write(str(err))
    sys.stdout.write(texto)
    sys.exit(err)
# ---------------------------------Opciones------------------------------------
# Requerimientos del sistema
paquetes = [
    'setuptools',
    'argparse>=1.4.0',
    'simpy>=3.0.10',
    'numpy>=1.12.1',
    'SIP>=4.18.1',
    'PyQt5>=5.7',
    'matplotlib>=2.0.0',
    'pyinstaller>=3.3',
]

clasificadores = [
    # Lenguaje y version:
    'Programming Language :: Python :: 3 :: Only',
    # "2 - Pre-Alpha", "3 - Alpha", "4 - Beta", "5 - Production/Stable"
    'Development Status :: 2 - Pre-Alpha',

    # Ambiente:
    'Environment :: Win32 (MS Windows)',
    'Environment :: X11 Applications',
    'Environment :: X11 Applications :: Qt',
    'Environment :: MacOS X',

    # Publico:
    'Natural Language :: Spanish',
    'Intended Audience :: Science/Research',
    'Intended Audience :: End Users/Desktop',
    'Intended Audience :: Education',
    'Topic :: Scientific/Engineering',
    'Topic :: Scientific/Engineering :: Bio-Informatics',
    'Topic :: Scientific/Engineering :: Information Analysis',
    'Operating System :: MacOS',
    'Operating System :: Microsoft',
    'Operating System :: POSIX',

    # Licencia:
    'License :: OSI Approved :: MIT License',
]

# -----------------------------------Setup-------------------------------------
setup(
    name='Bipylvo',
    # Basar las versiones según el estandar PEP440:
    # https://packaging.python.org/en/latest/single_source_version.html
    version=__version__,

    description='Simulador tipo Montecarlo de la fisiología de ' \
                'Bivalvos en PyQt5.',
    long_description=descripcionLarga,
    license='MIT',

    author='Luis J. Freites E.',
    author_email='sleyter45@gmail.net',
    url='https://gitlab.com/sleyter45/Bipylvo',

    # Ver https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=clasificadores,
    platforms=['X11', 'OS/X', 'Windows'],
    # Palabras clave:
    keywords='simulación montecarlo scope for growth bivalvo ' \
        'vieira euvola ziczac qt pyqt simpy numpy matplotlib',

    # Paquetes requeridos:
    packages=find_packages(exclude=['contrib', 'docs', 'tests', 'Recursos', ]),
    # Requerimientos: https://packaging.python.org/en/latest/requirements.html
    install_requires=paquetes,

    package_data={
        '': ['*.txt', '*.rst'],
        'bipylvo': ['*.msg'],
    },


    # Definición de los Scripts ejecutables del proyecto
    entry_points={
        'gui_scripts': ['Bipylvo = bipylvo.__main__:main', ],
    },

    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
)
