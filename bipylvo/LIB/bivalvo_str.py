#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Contiene el texto estándar a mostrar para los bivalvos."""

bivalvo_str = """-----------------------------------------------------
Simulación del bivalvo [ %s ]:[ %s ]
Tiempo simulado: %d días
Talla:[ %f ]cm / Masa Total húmeda:[ %f ]g / Nivel:[ %s ]

Masa Somática húmeda: [ %f ]g
Energía usada para Crecimiento Somático:[ %f ] J.
Energía usada para Crecimiento Concha:[ %f ] J.
Masa de Concha: [ %f ]g

Masa de Gónada húmeda: [ %f ]g
Energía usada para Producir Gónada:[ %f ] J. Total
Energía usada para Madurar la Gónada:[ %f ] J.

Energía perdida:[ %f ] J. Total
-----------------------------------------------------"""
