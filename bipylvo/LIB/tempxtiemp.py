#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Librería para funciones de estandarizar las temperaturas.

Debido al extenso manejo de listas, requiere la librería Numpy.
"""
# --------------------------Libs-Esenciales-----------------------------
from numpy import array, concatenate, linspace
# ----------------------------------------------------------------------


# -----------------Función-temperaturas---------------------------------
def temperaturas_para_el_tiempo(args):
    """Función para estandarizar las entradas de temperatura.

    Recibe los argumentos del main() con argparse y estandariza la lista de
    temperaturas para que siga una temperatura para cada día de simulación.
    Obligatorio: usar argparse para args.tiempo (días) y args.temperatura (°C).
    Parámetros: (args)
    """
    particion = 0
    temp_list = array(())
    temp_len = len(args.temperatura)

    if args.tiempo >= 1:
        if temp_len and type(args.temperatura) == list:
            if (temp_len > 1) and (args.tiempo > temp_len):
                particion = int(args.tiempo/(temp_len-1))
                q = int(args.tiempo % (temp_len-1))
                for idx, tiemp in enumerate(args.temperatura):
                    if (idx + 2) == temp_len:
                        if q > 0:
                            particion += q
                        temp_list = concatenate(
                            (
                                temp_list,
                                linspace(
                                    tiemp,
                                    args.temperatura[idx+1],
                                    int(particion),
                                    True,
                                ),
                            ),
                        )
                        break
                    else:
                        temp_list = concatenate(
                            (
                                temp_list,
                                linspace(
                                    tiemp,
                                    args.temperatura[idx+1],
                                    int(particion),
                                    False,
                                    ),
                            ),
                        )
            elif (temp_len == 1) and (args.tiempo > temp_len):
                temp_list = args.temperatura * args.tiempo
            else:
                temp_list = args.temperatura
        else:
            texto = 'La Temperatura del Ambiente esta vaciá o ' \
                'no cumple con el formato de lista.'
            raise Exception(texto)
    else:
        raise Exception('El tiempo a simular debe ser mayor o igual a 1 día.')
    return temp_list


# ----------------Función-temperaturas-Para-Diccionarios-(GUI)------------------
# Recibe los argumentos del main() mediante un diccionario y estandariza
# la lista de temperaturas para que siga una temperatura para cada día
# de simulación Obligatorio: tiempo (días) y temperatura (°C).
def temperaturas_para_el_tiempo_dic(args):
    """Función para estandarizar las entradas de temperatura, método gráfico.

    Recibe los argumentos del la ventana principal mediante un diccionario y
    estandariza la lista de temperaturas para que siga una temperatura para
    cada día de simulación.
    Obligatorio: usar diccionario para args[tiemp] (días) y args[temps] (°C).
    Parámetros: (args)
    """
    particion = 0
    temp_list = array(())
    temp_len = len(args["temps"])

    if args["tiemp"] >= 1:
        if temp_len and type(args["temps"]) == list:
            if (temp_len > 1) and (args["tiemp"] > temp_len):
                particion = int(args["tiemp"]/(temp_len-1))
                q = int(args["tiemp"] % (temp_len-1))
                for idx, tiemp in enumerate(args["temps"]):
                    if (idx + 2) == temp_len:
                        if q > 0:
                            particion += q
                        temp_list = concatenate(
                            (
                                temp_list,
                                linspace(
                                    tiemp,
                                    args["temps"][idx+1],
                                    int(particion),
                                    True,
                                ),
                            ),
                        )
                        break
                    else:
                        temp_list = concatenate(
                            (
                                temp_list,
                                linspace(
                                    tiemp,
                                    args["temps"][idx+1],
                                    int(particion),
                                    False,
                                ),
                            ),
                        )
            elif (temp_len == 1) and (args["tiemp"] > temp_len):
                temp_list = args["temps"]*args["tiemp"]
            else:
                temp_list = args["temps"]
        else:
            texto = 'Error: La Temperatura del Ambiente esta vaciá o ' \
                'no cumple con el formato de lista.'
            raise Exception()
    else:
        texto = 'Error: El tiempo a simular debe ser mayor o igual a 1 día.'
        raise Exception(texto)
    return temp_list
