"""
Archivo "estads.py" Contiene las funciones de el análisis estadístico.

Todas las funciones aquí presentes son implementadas a los resultados e una
simulación, los cuales son suministrados como un arreglo de objetos referentes
a los bivalvos simulados.
"""
# ----------------------------------------------------------------------


# ---------------------------------Libs---------------------------------
# import math

from numpy import mean, std
# ----------------------------------------------------------------------
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
def promediosYvarianzas(bivalvos=[]):
    """Función "promediosYvarianzas" genera estadísticas de los resultados.

    Las estadísticas se obtienen a partir de una lista de bivalvos. Si la lista
    esta vaciá se dispara un error del tipo "IndexError".
    """
    if not bivalvos or not len(bivalvos):
        raise IndexError("IndexError: Arreglo de bivalvos vació.")
    else:
        cantidad = len(bivalvos)
        unidades = bivalvos[0].unidades
        vivos = 0
        muertos = 0
        estadis = {
            'talla': list(), 'pesoT': list(), 'masaS': list(),
            'masaG': list(), 'pesoC': list(), 'EnrAG': list(),
        }

        for bivalvo in bivalvos:
            if bivalvo.ani['Estado']:
                vivos += 1
            else:
                muertos += 1
            estadis['talla'].append(bivalvo.dat["Tall"])
            estadis['pesoT'].append(bivalvo.Mas_total())
            estadis['masaS'].append(bivalvo.dat['Mas-So'])
            estadis['masaG'].append(bivalvo.dat['Mas-Go'])
            estadis['pesoC'].append(bivalvo.dat['Mas-Sh'])

        return (
            "Bivalvos Vivos: %d (%.2f %%)" % (
                vivos, float(vivos*100)/cantidad,
            ),
            "Bivalvos Muertos: %d (%.2f %%)" % (
                muertos, float(muertos*100)/cantidad,
            ),
            "Promedio de las Tallas: %.4f %s." % (
                mean(estadis['talla']),
                unidades['talla'][0],
            ),
            "Desv. Estándar de las Tallas: %.4f %s." % (
                std(estadis['talla']),
                unidades['talla'][0],
            ),
            "Promedio de la Masa Total: %.4f %s." % (
                mean(estadis['pesoT']),
                unidades['peso'][0],
            ),
            "Desv. Estándar de la Masa Total: %.4f %s." % (
                std(estadis['pesoT']),
                unidades['peso'][0],
            ),
            "Promedio de la Masa Concha: %.4f %s." % (
                mean(estadis['pesoC']),
                unidades['peso'][0],
            ),
            "Desv. Estándar de la Masa Concha: %.4f %s." % (
                std(estadis['pesoC']),
                unidades['peso'][0],
            ),
            "Promedio de la Masa Somática: %.4f %s." % (
                mean(estadis['masaS']),
                unidades['peso'][0],
            ),
            "Desv. Estándar de la Masa Somática: %.4f %s." % (
                std(estadis['masaS']),
                unidades['peso'][0],
            ),
            "Promedio de la Masa Gónada: %.4f %s." % (
                mean(estadis['masaG']),
                unidades['peso'][0],
            ),
            "Desv. Estándar de la Masa Gónada: %.4f %s." % (
                std(estadis['masaG']),
                unidades['peso'][0],
            ),
        )

# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# ----------------------------------------------------------------------
