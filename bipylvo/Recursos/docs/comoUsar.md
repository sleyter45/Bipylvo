## Uso del simulador:

Para usar el simulador es necesario una observar que el programa se divide en tres (`3`) pestañas:

1.  `Escenario`: contiene todos los parámetros necesarios para realizar una simulación, es la pestaña inicial del simulador permite definir los valores iniciales de una simulación y posteriormente ejecutar una o mas instancias del bivalvo seleccionado en los parámetros del escenario. Para ejecutar una simulación el usuario (usted) debe definir los siguientes valores:

    1.  `Especie a Simular`: es un selector desplegable que contiene la lista de bivalvos que pueden ser simulados en este sistema. Es necesario definir el bivalvo a simular para poder asignar temperaturas, cantidades de alimento, y para ejecutar simulaciones.

    2.  `Grupo Etario`: es un selector desplegable que contiene los grupos etarios que poseé la especie seleccionada para poder simular a un bivalvo que comprenda las características designadas para cada uno de estos grupos. Debe elegirse una especie a simular para que el sistema cargue la lista de grupos etarios disponibles.

    3.  `Alimento (%)`: es la cantidad de alimento que se le asigna diariamente a un bivalvo para su consumo. El alimento es un numero real positivo, mayor a cero (`0`), y representa el porcentaje del peso somático del animal que debe ser asignado como alimento diariamente.

    4.  `Tiempo (Días)`: es un numero entero que determina la cantidad de días que deben ser simulados. Se debe resaltar que cada animal a simular va a cumplir un numero de ciclos o días de simulación igual a los que se coloquen en esta variable.

    5.  `Cantidad (Bivalvos)`: es un numero entero que permite asignar el numero de entidades aleatorias que van a ser ejecutadas. Cada entidad esta aislada de la otra (como si fuera un bivalvo por cada pecera).

    6.  `Temperaturas (°C) para la simulación`: Es una lista de temperaturas a usar durante la simulación, la lista permite al usuario determinar como fluctuará las temperaturas entre el comienzo y el final de la misma lista. Las variaciones se hacen de forma lineal. Para agregar o remover temperaturas a la lista debe usar los siguientes botones disponibles:
        >   Nota: Actualmente no se puede organizar o filtrar las temperaturas registradas.

        1.  `Agregar`: permite agregar una (`1`) o mas temperaturas para la simulación mediante el uso de una ventana desplegable que tiene un método de entrada del tipo numérico real positivo, con limites designada según la `Especie a Simular` seleccionada, por lo que debe seleccionar la especie antes de agregar temperaturas a la lista. Actualmente solo puede agregar una temperatura al final de la lista.

        2.  `Eliminar`: permite eliminar una (`1`) temperatura seleccionada en el listado de temperaturas. Solo puede eliminar una temperatura a la vez.

    7.  `Iniciar Simulación`: ejecuta una validación de los parámetros del escenario de simulación, y de estar dentro de los parámetros permitidos se ejecuta una simulación. La ejecución de una simulación mostrará una ventana de carga que permite cancelar la ejecución. Posteriormente redirige al usuario a la pestaña de `Simulación`.

2.  `Simulación`: la sección de simulación se divide en tres (`3`) secciones que permiten observar los resultados y/o parámetros de la simulación realizada. La tabla de resultados es el componente principal y de mayor tamaño asignado en la ventana, dicha tabla contiene los valores resultantes de la simulación determinados por la especie a simular. Al ejercer clic en una de las filas de la tabla el simulador permite al usuario acceder directamente a la pestaña de `Bivalvo Seleccionado` cargando los datos del bivalvo seleccionado.
    >   Nota: Siempre es posible saltar entre cualquiera de las pestañas existentes, incluso cuando no se tienen datos simulados.

3.  `Bivalvo Seleccionado`: esta es la sección de detalles, que permite observar con comodidad los valores de los atributos del bivalvo seleccionado, para un día de simulación especificado. La pestaña cuenta con los siguientes componentes:

    1.  Selección del bivalvo y día: la sección cuenta con dos (`2`) selectores que permiten buscar por Identificador (`ID`) y por `Día`, los cuales despliegan los cambios cuando sus valores son modificados.

    2.  La pestaña de `resultados`: contiene una lista con los variables y acumuladores del bivalvo seleccionado para el día de simulación especificado anteriormente, aquí puede observar con detalle los cambios de.

    3.  La pestaña de `eventos`: contiene los registros de los sucesos relevantes en la simulación del bivalvo seleccionado. El listado es completo y no se ve limitado según el `Día` seleccionado.

        >   Navegar entre estas pestañas no afecta a las pestañas principales o a los controles del selector de bivalvos.

    4.  El selector de gráficas: titulado `Gráficas Disponibles`, permite elegir entre las gráficas lineales que pueden generarse con este simulador. Una vez seleccionada una gráfica de la lista, el programa abre una ventana y creando la gráfica, con las opciones disponibles para su manejo.

#### Ejemplo de uso:
1.  Inicia el programa.

2.  Elije una especie de bivalvo y después prosigue a llenar el resto de los parámetros.

    | Campo             | Valores                               |
    |-------------------|---------------------------------------|
    | 1. Especie:       | `Euvola ziczac Tropical`              |
    | 2. Grupo Etario:  | `1) Juveniles`                        |
    | 3. Alimento:      | `6` %                                 |
    | 4. Tiempo:        | `30` días                             |
    | 5. Cantidad:      | `20` vieiras                          |
    | 6. Temperatura:   | Agregar `20` °C y salir del dialogo.  |

3.  Ejecuta la simulación.

4.  Observa los resultados.

5.  Elige un bivalvo y leé los detalles de interés.

6.  Navega entre los días de simulación observando los datos y sus cambios.

7.  Puede concluir el ejercicio.
