# PROYECTO BIPYLVO
[![Bipylvo.ico](bipylvo-logox128.png)](https://gitlab.com/sleyter45/Bipylvo/)  [![DLI-UDONS](LogoInfo.png)](http://herbario.sucre.udo.edu.ve/index.php) [![UDONS](logo-udo.png)](http://www.sucre.udo.edu.ve/)

[Bipylvo](https://gitlab.com/sleyter45/Bipylvo/) es un proyecto de desarrollo de un modelo de simulación capaz de emular el comportamiento fisiológico de la vieira *Euvola ziczac*, basado en modelos matemáticos como la Formula del Balance Energético (SFG) y la ecuación de crecimiento de *von Bertalanffy*, además del conocimiento  sobre el desarrollo y comportamiento del bivalvo *Euvola ziczac* obtenido en el Centro de Investigaciones en Biología y Acuicultura de Moluscos (CIBAM) de la [Universidad de Oriente Núcleo de Sucre (UDONS)](http://www.sucre.udo.edu.ve/). El modelo es aplicado para el desarrollo y ejecución de un Software de simulación discreto del tipo **Montecarlo** y aplicando la metodología de desarrollo Proceso de Solución de Problemas en CA/IO. Para esto se emplearon el lenguaje de programación general *Python*, además de la librería *Simpy* para el manejo de los ciclos de simulación, con una interfaz gráfica basada en la librería *Qt 5* y sus respectiva  implementación para el lenguaje llamada *PyQt5*; además de la capacidad para generar gráficas estadísticas usando la librería *Matplotlib*. Este software está desarrollado como un Software Libre con propósitos académicos, entre ellos, la de servir de herramienta de apoyo para la investigación y enseñanza en el Centro de Investigación de la Biología y Acuicultura de moluscos (CIBAM).

## Objetivo del Simulador:
El propósito principal del simulador es permitir al usuario **realizar la ejecución de la formula del Balance Energético o S.F.G.(*Scope for Growth*)** en las especies de bivalvos que se tengan a disposición, sin embargo el sistema nació para de simular el Balance Energético de la vieira *Euvola ziczac*

### La Fórmula del Balance Energético o *Scope for Growth (SFG)*:
Es un modelo matemático que permite observar el equilibrio alcanzado por el organismo entre la energía obtenida a través del alimento, y la perdida debido al gasto metabólico, evidenciado mediante la respiración y la excreción. Una vez satisfecho esto, la energía remanente puede ser usada para el desarrollo, crecimiento, reproducción, entre otros, de cada individuo. El trabajo de Widdows & Johnson (1988) muestra el modelo como:
```P = A - (R + U)```
  > Donde “`P`” representa la energía sobrante y también es representada como **S.F.G.**, “`A`” representa la energía absorbida del alimento ingerido, “`R`” gasto energético en la respiración, y “`U`” representa la energía perdida como excreta. Los resultados son mostrados en unidades de energía (Joules) y el modelo puede ser aplicado para cualquier bivalvo.

Mediante el empleo del **S.F.G.** se puede apreciar la cantidad energía obtenida por el bivalvo bajo estudio, y que luego sería distribuida por el organismo. La asignación de energía entre el mantenimiento, crecimiento y reproducción es una parte central de la estrategia de la vida (Cody, 1966; Levins, 1968; Sibly & Calow, 1986; Williams, 1966a, b). Por supuesto, la asignación de energía es un proceso dinámico que depende de una multitud de factores ecológicos y fisiológicos (Sibly & Calow, 1986; Stearns, 1992).

## Acerca del Autor:
Creado por [*Luis J. Freites E.*](mailto://sleyter45@gmail.com), [Bipylvo](https://gitlab.com/sleyter45/Bipylvo/) es un software de simulación para entorno gráfico, de propósito de investigación y educativo, desarrollado como parte de la tesis de grado en la [Universidad de Oriente Núcleo de Sucre (UDONS)](http://www.sucre.udo.edu.ve/), para la [Departamento de Licenciatura en Informática](http://herbario.sucre.udo.edu.ve/index.php) y el Centro de Investigaciones en Biología y Acuicultura de Moluscos (CIBAM).

### Licencia:
 MIT [ver: LICENSE.txt](https://gitlab.com/sleyter45/Bipylvo/raw/master/LICENSE.txt)

  > **Software Libre, pero el copyright queda para la UDONS debido lo estipulado en el reglamento de Trabajo de Pregrado**
