#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Archivo "eziczac.py".

Bipylvo es un proyecto experimental con el fin de crear un software para la
Simulación del gasto energético en distintas etapas del ciclo de vida de la
vieira "Euvola ziczac", donde dicho comportamiento es definido mediante la
recopilación de datos e investigaciones, y la observación empírica en el medio
ambiente.

La finalidad de este documento es crear una representación fiable del
comportamiento fisiológico de la Vieira Euvola ziczac. Donde la clase
"Eziczac_tropical" define los parámetros de estudio de este animal y su
proceso de simulación en un ambiente laboratorio. Cabe destacar que el
comportamiento de esta clase es distinto al de un individuo de la especie
que se desarrolle en climas templados.
"""
# -------------------------------Libs-----------------------------------
from __future__ import unicode_literals

from copy import deepcopy

from math import exp, log

from LIB.bivalvo_str import bivalvo_str

from numpy import random
# ----------------------------------------------------------------------


# ----------------------Animal-Eziczac-Tropical-------------------------
class Eziczac_tropical(object):
    """Clase "Eziczac_tropical". Representa a un Euvola ziczac tropicalizado.

    Esta clase sirve para representar la información relevante de la vieira y
    su comportamiento a simular. El proceso de simulación de este animal esta
    definido en la función "simular_acuario" la cual ejecuta un ciclo cuyas
    iteraciones representan un (1) día en la vida del animal definido y son
    controladas por el entorno o ambiente definido por la librería Simpy. El
    método experimental de la simulación requiere:
      - Colocar (1) solo animal (vivo (estado=1)) por cada acuario.
      - Colocar alimento 1 vez al día, con una cantidad equivalente a un
        porcentaje del peso del tejido orgánico del animal (máximo 8% del peso
        para la E. ziczac)
      - Definir el grupo etario al que pertenece el animal, y las diferencias
        entre cada grupo.
      - Limpiar el agua del acuario una vez cada día, para no acumular datos de
        variables de los días anteriores.

    En cada día de simulación se realiza un calculo conocido como "Scope for
    Growth" el cual es necesario para determinar el gasto energético del animal
    y cuales son las ganancias de energía que pueden ser invertidas en su
    desarrollo, pero para determinar las ganancias y costos en energía que
    presenta el animal a lo largo de la simulación se usan variables aleatorias
    provenientes de la librería Numpy, la cual usa los parámetros obtenidos
    para éste y otros animales a lo largo de múltiples investigaciones. Cabe
    destacar que dichos parámetros han sido ajustados para el caso de la Vieira
    en un clima tropical.

    En este simulador se usa el sistema métrico y Joules para los valores de
    energía.
    """

    nombre = "Euvola ziczac Tropical"
    # ----------------------------Constantes----------------------------
    # GE = {'Tall':'cm','Mas':'gr','ShellMas':'gr'} (Lodeiros et.al 1996
    # gráficas-pag247, datos interpretados y arreglados para hacer grupos
    # "continuos", todo en conjunto con el análisis de los datos de muestreos
    # en bancos naturales realizado para obtener la formula de "vonBert").
    # El tamaño máximo de una adulta [M][TMax] es conocido como L-infinito
    # Grupos etarios del animal.
    etapas = {
        'J': {
                'Tmin': 1.65, 'Tmax': 3.8, 'Mmin': 1.0,
                'Mmax': 5.0, 'ShMmin': 1.0, 'ShMmax': 5.0,
            },
        'I': {
                'Tmin': 3.9, 'Tmax': 5.5, 'Mmin': 5.1,
                'Mmax': 11.0, 'ShMmin': 5.1, 'ShMmax': 12.0,
            },
        'M': {
                'Tmin': 5.6, 'Tmax': 8.0418954593, 'Mmin': 11.1,
                'Mmax': 50.0, 'ShMmin': 12.1, 'ShMmax': 50.261846620625,
            },
    }
    # Se definen los nombres textuales de los grupos etarios, los números son
    # para implementar un ordenamiento a la lista de claves.
    etapas_nomb = {
        "1) Juveniles": 'J',
        "2) Juveniles Maduros": 'I',
        "3) Adultos": 'M',
    }
    # Grupos etarios con actividad reproductiva
    etapas_reproductivas = ('I', 'M')
    # Limites de temperatura y comida:
    limites = {'tempMax': 30, 'tempMin': 20, 'alimMax': 8, 'alimMin': 0.1}
    # Unidades en uso:
    unidades = {
        'peso': ("gr", "Gramos"),
        'energ': ("J", "Joules"),
        'talla': ("cm", "Centímetros"),
    }
    # Cabeceras para las tablas de datos en las GUI y archivos.
    # nota: la función self.tabulado() debe coincidir con estos datos.
    cabeceras_para_tablas = (
        "Identificador",
        "Especie",
        "Estado",
        "Días",
        "Grup. Etario",
        "Talla (dorso-ventral)[cm]",
        "Peso Total[gr]",
        "Peso Concha[gr]",
        "Peso Tejido[gr]",
        "Peso Gónada[gr]",
        "Energía Somat.[J]",
        "Energía Conch.[J]",
        "Energía P.Gon.[J]",
        "Energía M.Gon.[J]",
        "Energía Perd.[J]",
    )

    # Constante de crecimiento mensual (k) del bivalvo necesaria para la
    # formula de von Bertalanffy.
    # Nota: esta variable fue tomada del análisis de la pendiente de los datos
    # obtenidos de muestreos mensuales en investigaciones realizadas por el
    # Instituto Océano Gráfico de la Universidad de Oriente Núcleo de Sucre.
    k = 0.422522496

    # ------------------------------Constructor---------------------------------
    def __init__(self, ID, T, A, N):
        """Constructor __init__ de la clase "Eziczac_tropical".

        Crea la instancia de datos para un individuo de la Vieira E. ziczac.
        Recibe un parámetro Identificador (ID) que distingue al animal y debe
        ser un numero entero, seguido por la lista de temperaturas a usar
        (una (1) por cada día de simulación), el porcentaje de alimento a
        suministrar al animal, y por ultimo el grupo etario del bivalvo.

        Parámetros: (self, ID, T, A, N)
        """
        # Nota Siempre usar un numero como ID. Solo Números.
        self.amb = {'Temp': T, 'Alim': A, 'Ener': 23, }
        # Tiempo de inanición variable entre 12 a 14 días con poco alimento,
        # para casos de no ganar suficiente energía como para satisfacer sus
        # costos metabólicos básicos en un tiempo prolongado.
        # self.amb["Tiemp_Inan"] = random.random_integers(low=12, high=14)
        # Nota: Tiempo de inanición (máximo) eliminado del modelo.

        if not str(ID).isdigit():
            raise Exception(
                "Error. El ID o identificador del bivalvo debe ser un número.",
            )

        self.ani = {'ID': int(ID), 'Niv': N, 'Estado': 1}
        self.dat = {'Tall': 0, 'Mas-So': 0, 'Mas-Go': 0,
                    'Mas-Sh': 0, 'peso_org_inan': 0, }
        self.ener = {'ECrec-Sh': 0, 'ECrec-So': 0, 'EPgon': 0,
                     'EAgon': 0, 'EPerd': 0, 'EMgon': 0, }
        self.even = list()
        self.dias = 0

        # Agregar diccionario de funciones para crear gráficas:
        self.graficas = {
            "Gráficar Talla": self.graf_talla,
            "Gráficar Peso Somático": self.graf_peso_soma,
            "Gráficar Peso de la Concha": self.graf_peso_conc,
            "Gráficar Peso de T. Gónada": self.graf_peso_gona,
            "Gráficar Energía en T. Somático": self.graf_ener_soma,
            "Gráficar Energía en la Concha": self.graf_ener_conc,
            "Gráficar Energía en Producir Gónada": self.graf_ener_Pgona,
            "Gráficar Energía en Madurar la Gónada": self.graf_ener_Agona,
            "Gráficar Perdidas de Energía": self.graf_ener_perd,
        }

        # Aleatoriedad del tamaño máximo del molusco: (a L-infinito se le
        # suma un numero aleatorio generado por una variable aleatoria
        # normal con promedio 0 y varianza 0.3)
        self.etapas['M']['Tmax'] += random.normal(0, 0.3)

        a = b = c = d = e = f = 0
        if self.ani['Niv'] == 'J':
            a = self.etapas['J']['Tmin']
            b = self.etapas['J']['Tmax']
            c = self.etapas['J']['Mmin']
            d = self.etapas['J']['Mmax']
            e = self.etapas['J']['ShMmin']
            f = self.etapas['J']['ShMmax']
        elif self.ani['Niv'] == 'I':
            a = self.etapas['I']['Tmin']
            b = self.etapas['I']['Tmax']
            c = self.etapas['I']['Mmin']
            d = self.etapas['I']['Mmax']
            e = self.etapas['I']['ShMmin']
            f = self.etapas['I']['ShMmax']
        elif self.ani['Niv'] == 'M':
            a = self.etapas['M']['Tmin']
            b = self.etapas['M']['Tmax']
            c = self.etapas['M']['Mmin']
            d = self.etapas['M']['Mmax']
            e = self.etapas['M']['ShMmin']
            f = self.etapas['M']['ShMmax']
            self.dat['Mas-Go'] = random.uniform(0, 1.75)
        else:
            raise Exception(
              'Las Tallas de los animales deben estar en el rango: J, I, o M',
            )

        randstate = random.get_state()
        self.dat['Tall'] = random.uniform(a, b)
        random.set_state(randstate)
        self.dat['Mas-So'] = random.uniform(c, d)
        random.set_state(randstate)
        self.dat['Mas-Sh'] = random.uniform(e, f)

        # Calcular la edad aproximada según su talla:
        mesesAdias = self.talla_a_tiempo(self.dat['Tall'])
        if not mesesAdias > 0:
            mesesAdias *= 30
        self.edadAprox = mesesAdias  # valor en día

        self.ini = deepcopy(self)
        self.ini.ani['ID'] = str(self.ini.ani['ID']) + '-Inicial'
        self.hist = list()

        return

    # ------------------------------------------------------------------
    def __deepcopy__(self, memo):
        """Función "deepcopy" modificada para el objeto Eziczac_tropical."""
        cls = self.__class__
        resultado = cls.__new__(cls)
        memo[id(self)] = resultado
        for k, v in self.__dict__.items():
            if (k != 'hist') and (k != 'even'):
                setattr(resultado, k, deepcopy(v, memo))

        setattr(resultado, 'hist', None)
        setattr(resultado, 'even', None)
        return resultado

    # ------------------------------------------------------------------
    def Mas_total(self):
        """Función "Mas_total" muestra la masa total acumulada del bivalvo."""
        return self.dat['Mas-So']+self.dat['Mas-Go']+self.dat['Mas-Sh']

    # ------------------------------------------------------------------
    def alim(self):
        """Función "alim" calcula la cantidad de alimento a suministrar."""
        return (
            ((self.dat['Mas-So'] + self.dat['Mas-Go'])
             * (float(self.amb['Alim']) / 100)) * 1000
        )

    # ------------------------------------------------------------------
    def pesoseco_alim(self, peso):
        """Función "pesoseco_alim" calcula el peso seco del alimento.

        Los investigadores consultados indicaron que el alimento se reduce
        hasta solo quedar un 14% (aproximadamente) del peso húmedo del
        alimento.

        Parámetros: (self, peso)
        """
        return peso*0.14

    # ------------------------------------------------------------------
    def pesoseco_tejido(self, peso):
        """Función "pesoseco_tejido" calcula el peso seco del tejido vivo.

        Transforma el peso húmedo del tejido a peso seco (9%% del peso húmedo).

        Parámetros: (self, peso)
        """
        return peso*0.09

    # ------------------------------------------------------------------
    def pesohumedo_tejido(self, peso):
        """Función "pesohumedo_tejido" calcula el peso húmedo del tejido seco.

        Transforma el peso seco de un tejido a peso húmedo.

        Parámetros: (self, peso)
        """
        return (peso*100)/9

    # ------------------------------------------------------------------
    def actualizar_nivel(self):
        """Función "actualizar_nivel" actualiza el grupo etario del bivalvo.

        Realiza cambios en el grupo etario de la vieira según la talla actual
        de la misma o el numero de días de vida que se calcule aproximadamente
        al inicio de la simulación, sumado a los días ya simulados.

        El valor por defecto es Juvenil (J).
        """
        if (self.dat['Tall'] >= self.etapas['M']['Tmin']):
            self.ani['Niv'] = 'M'
        elif ((self.edadAprox + self.dias) > 150) and \
             (not self.edadAprox <= 0):
            self.ani['Niv'] = 'M'
        elif (self.dat['Tall'] < self.etapas['M']['Tmin']) and \
             (self.dat['Tall'] >= self.etapas['J']['Tmax']):
            self.ani['Niv'] = 'I'
        elif ((self.edadAprox + self.dias) > 65) and \
             (not self.edadAprox <= 0):
            self.ani['Niv'] = 'I'
        else:
            self.ani['Niv'] = 'J'
        return

    # ------------------------------------------------------------------
    def gon_to_energ(self):
        """Función "gon_to_energ" transforma de peso seco de gónada a energía.

        Toma el peso húmedo actual de la gónada, lo transforma a peso seco, y
        después lo transforma a su equivalente energético: 1g == 26kj == 26000j
        Los equivalentes energéticos son extraídos de:
            - MacDonald and Thompson 1985b
        """
        return (self.pesoseco_tejido(self.dat['Mas-Go']) * 26000)

    # ------------------------------------------------------------------
    def energ_to_gon(self, energ):
        """Función "energ_to_gon" convierte energía a tejido de la gónada.

        Toma el parámetro energía (energ) y lo transforma a tejido vivo de la
        gónada del bivalvo. Equivalentes: 1g == 26kj == 26000j
        Los equivalentes energéticos son extraídos de:
            - MacDonald and Thompson 1985b

        Parámetros: (self, energ)
        """
        return float(energ)/26000

    # ------------------------------------------------------------------
    def soma_to_energ(self, peso=None):
        """Función "soma_to_energ" transforma de peso somático seco a energía.

        Toma el peso húmedo actual del tejido somático, lo transforma a peso
        seco, y después lo transforma a su equivalente energético:
            1g == 24.5kj == 24500j
        Los equivalentes energéticos son extraídos de: Thompson 1977.

        Si el parámetro "peso" contiene un valor distinto de None (Null),
        entonces se retornará el equivalente de energía para ese peso.
        No es necesario usar "pesoseco_tejido" previamente.
        """
        if peso is not None:
            return (self.pesoseco_tejido(peso) * 24500)
        else:
            return (self.pesoseco_tejido(self.dat['Mas-So']) * 24500)

    # ------------------------------------------------------------------
    # Toma energía y lo transforma a tejido somático húmedo.
    def energ_to_soma(self, energ):
        """Función "energ_to_soma" convierte energía a tejido de somático.

        Toma el parámetro energía (energ) y lo transforma a tejido somático
        vivo del bivalvo. Equivalentes: 1g == 24.5kj == 24500j
        Los equivalentes energéticos son extraídos de: Thompson 1977

        Parámetros: (self, energ)
        """
        return float(energ)/24500

    # ------------------------------------------------------------------
    def energ_to_shell(self, energ):
        """Función "energ_to_shell" convierte energía a crear concha (masa).

        Toma el parámetro energía (energ) y lo emplea para crear la concha del
        bivalvo, pero este cambio solo se refleja como un aumento de peso.
        Equivalentes: 1g == 21.1kj == 21100j
        Los equivalentes energéticos son extraídos de: Hughes 1970

        Parámetros: (self, energ)
        """
        return (float(energ)/21100)

    # ------------------------------------------------------------------
    # Toma energía y lo transforma a Talla de la concha.
    def energ_to_Tshell(self, energ):
        """Función "energ_to_Tshell" convierte energía a crear concha (talla).

        Toma el parámetro energía (energ) y lo emplea para crear la concha del
        bivalvo, pero este cambio solo se refleja como un aumento de talla
        dorso-ventral.
        Equivalentes: (71mm = 11kj => 7.1cm = 11000j) (Talla dorso-ventral)
        Los equivalentes energéticos son extraídos de: kesarcodi-watson 2001

        Parámetros: (self, energ)
        """
        return (float(energ * 7.1)/11000)

    # ------------------------------------------------------------------
    def h_a_dia(self, dat):
        """Función "h_a_dia" toma un dato/hora y lo pasa a 24horas (1 día).

        Transforma los datos que las formulas muestran por hora a diarios.
        Parámetros: (self, dat)
        """
        return dat * 24

    # ------------------------------------------------------------------
    def Effi_Abs(self):
        """Variable aleatoria para la Eficiencia de Absorción."""
        return random.normal(
            ((self.amb['Temp'][self.dias] * 55 / 24) +
             (self.pesoseco_tejido(self.dat['Mas-So']) * 5 / 5.78)
             ), 1.5,
        )

    # ------------------------------------------------------------------
    def Energ_AbsxD(self, peso, EA):
        """Función "Energ_AbsxD" calcula el alimento obtenido y la energía.

        Transforma el alimento en el ambiente (indicado en los parámetros como
        la variable "peso") a energía, según la eficiencia de absorción
        indicada en los parámetros como "EA".

        Parámetros: (self, peso, EA)
        """
        # Energía absorbida en el Día completo.
        return (self.pesoseco_alim(peso) * self.amb['Ener']) * (EA / 100)

    # ------------------------------------------------------------------
    def Resp_RatexH(self):
        """Variable Aleatoria para el gasto energético en la respiración.

        Gasto por Hora (necesita ser transformado al completo día)
        """
        return random.normal(
            ((self.amb['Temp'][self.dias]*6.5/24) +
             (self.pesoseco_tejido(self.dat['Mas-So']) *
             0.3 / 5.78)), 0.2,
        )

    # ------------------------------------------------------------------
    def Perd_ExcrxH(self, A):
        """Variable Aleatoria para el gasto energético en la excreta.

        Gasto por Hora (necesita ser transformado al completo día), necesita
        el parámetro "Energía del Alimento" (A) obtenido el mismo día.
        Parámetros: (self, A)
        """
        return random.normal(
            ((self.amb['Temp'][self.dias] * 0.9 / 24) +
             ((A / 24) * 0.8 / 12.5)), 0.07,
        )

    # ------------------------------------------------------------------
    def SFGxD(self, A, R, U):
        """Función "SFGxD" ejecución del Scope for Growth del día.

        Ejecuta la formula del balance energético para la vieira, retornando
        una la cantidad de energía que puede interpretarse como ganancia o
        perdida según su signo. Los valores positivos indican que el animal
        obtuvo suficiente energía para sostener sus gastos metabólicos, de lo
        contrario el resultado negativo repercute en sus reservas.
        Requiere la la energía obtenida del alimento (A), el gasto de energía
        en respiración (R), y la energía perdida en excreción (U).
            SFG = A - (R + U)

        Parámetros: (self, A, R, U)
        """
        return A - (self.h_a_dia(R) + self.h_a_dia(U))

    # ------------------------------------------------------------------
    def vonBert(self, meses):
        """
        Función "vonBert" determina el valor de la gráfica de von Bertanlaffy.

        Ejecuta la ecuación de von Bertanlaffy y su resultado es usado para
        determinar la cantidad de energía a invertir en el crecimiento de la
        vieira. Requiere el parámetro "meses" que simboliza la edad actual del
        individuo simulado, y se expresa en meses.
            Talla_en_Tiempo = Talla_máxima*(1-exponencial((-k*Tiempo_meses)))
        Parámetros: (self, meses)
        """
        return self.etapas['M']['Tmax'] * (1 - exp((-1 * self.k * meses)))

    # ------------------------------------------------------------------
    def talla_a_tiempo(self, talla):
        """Función "talla_a_tiempo" despeje de Tiempo en von Bertalanffy.

        Ejecuta la función de von Bertalanffy despejada para retornar la edad
        actual del bivalvo simulado en meses, usando su talla dorso ventral
        (talla) como parámetro. Si el animal a alcanzado su talla maxíma
        entonces el logaritmo dispara una excepción de valor fuera del
        dominio, por lo cual se captura la excepción y se retorna -1.
            Tiempo_meses = (-1/k)*log(((Talla_máxima-Talla)/Talla_máxima))
        Parámetros: (self, talla)
        """
        try:
            return (-1/self.k)*log(
                ((self.etapas['M']['Tmax']-talla)/self.etapas['M']['Tmax']),
            )
        except Exception:
            return -1

    # ------------------------------------------------------------------
    def Eng_Crec_shell(self, meses=-1):
        """Función "Eng_Crec_shell" asignación de energía para el crecimiento.

        Determina el porcentaje de energía a usar en el crecimiento del bivalvo
        siguiendo el comportamiento de la función de von Bertalanffy adecuada
        para la vieira E. ziczac. Requiere la edad de la vieira en "meses".
            porcentaje = 30*exponencial(-k*Tiempo_meses)
        Si el valor de la función es negativo, el bivalvo no destinará energía
        al crecimiento.
        Parámetros: (self, meses=-1)
        """
        if meses > 0:
            return 30*exp(-1*self.k*meses)
        else:
            return 0

    # ------------------------------------------------------------------
    def reg_evento(self, evento="No Asignado"):
        """Registra el evento señalado en el formato evento:día.

        Parámetros: (self, evento="No Asignado")
        """
        self.even.append("%s:%d" % (str(evento), self.dias + 1))
        return

    # ------------------------------------------------------------------
    def simular_acuario(self, env):
        """Función "simular_acuario" ejecuta la simulación del animal.

        Aquí se define el proceso de simulación que define el comportamiento de
        la vieira siguiendo los lineamientos del diseño experimental explicados
        anteriormente, es decir, se desarrolla el comportamiento del animal en
        un ambiente cerrado, con limpiezas de acuario cada día (no se conservan
        valores de variables ambientales entre días). Necesita como parámetro
        obligatorio el entorno o ambiente (env) de simulación de Simpy, este le
        permite determinar el tiempo de ejecución del proceso.
        Parámetros: (self, env)
        """
        # Aquí inicia la simulación.
        # Variables iniciales de la simulación.
        c_temp_Reprod = 0
        ult_Reprod = 0
        dias_inan = 0

        # Inicio del Ciclo
        while self.ani['Estado'] != 0:

            # Inicialización del Día.
            a = self.alim()
            EA = self.Effi_Abs()
            A = self.Energ_AbsxD(a, EA)
            R = self.Resp_RatexH()
            U = self.Perd_ExcrxH(A)

            # Scope for Growth
            SFG = self.SFGxD(A, R, U)

            # Agotamiento post-desove
            if (ult_Reprod > 0) and (SFG >= 0):
                ult_Reprod -= 1
                SFG = 0
                self.reg_evento('AgotadoxDesove')

            # Contador de días en inanición
            if SFG >= 0:
                if dias_inan > 0:
                    dias_inan -= 1
                else:
                    self.dat['peso_org_inan'] = 0
            else:
                # if dias_inan < self.amb['Tiemp_Inan']:
                dias_inan += 1
                #    if ult_Reprod > 0:
                #        dias_inan += 1
                # else:
                #    # Muerte si no tiene alimento en x = Tiempo Inanición
                #    self.ani['Estado'] = 0
                #    self.reg_evento('Muerte')

            # Evento Desove. (Reproducción)
            # Debe haber pasado el periodo de agotamiento, debe tener un SFG+,
            # debe estar en una etapa de vida con actividad reproductiva
            if (SFG > 0) and (self.ani['Niv'] in self.etapas_reproductivas):

                # Conteo de condiciones de desove
                # (debe sumar 400°C para desovar)
                if self.amb['Temp'][self.dias] >= 26:
                    c_temp_Reprod += self.amb['Temp'][self.dias]
                elif (self.amb['Temp'][self.dias] < 26) and \
                     (c_temp_Reprod > 0):
                    c_temp_Reprod -= self.amb['Temp'][self.dias]
                    if c_temp_Reprod < 0:
                        c_temp_Reprod = 0

                # Condiciones de ejecución de un desove sumado a las
                # condiciones previas
                # Al sumar 400 se ejecuta el desove. (A. Vélez, et.al. 1993)
                # + Una cantidad mínima de peso en gónada (10%)
                # + Una gónada madurada (Energía Activación Gónada)
                # Nota: mediante pruebas un bivalvo solo crea una gónada
                # de 10% aprox. de su peso corporal en 15 días a
                # 6% de alimento.
                mas_tegido = self.dat['Mas-Go'] + self.dat['Mas-So']
                if c_temp_Reprod >= 400 and \
                   self.dat['Mas-Go'] >= (mas_tegido*0.1) and \
                   self.ener['EAgon'] >= (self.gon_to_energ() / 2):

                    c_temp_Reprod = 0
                    ult_Reprod = 7  # Periodo de agotamiento (0 ganancias)
                    self.reg_evento('Desove')
                    self.dat['Mas-Go'] = 0
                    self.ener['EAgon'] = 0
                    SFG = 0

            # Aplicación del Scope For Growth
            if SFG > 0:
                # Calculo de ganancias.
                energ_pos = {'ECrec-Sh': 0, 'ECrec-So': 0, 'EPgon': 0}

                # Calculo de la asignación de energía al crecimiento
                porcentaje_crecimiento = self.Eng_Crec_shell(
                        self.talla_a_tiempo(self.dat['Tall'])) / 100

                energ_pos['ECrec-Sh'] = SFG * porcentaje_crecimiento
                self.ener['ECrec-Sh'] += energ_pos['ECrec-Sh']

                # Calculo de la energía para crecimiento somático y
                # reproducción
                SFG_sincrec = SFG - energ_pos['ECrec-Sh']

                if self.ani['Niv'] == 'J':
                    # Juveniles dedican toda la energía a crecimiento de concha
                    # y t.somático.
                    self.ener['ECrec-So'] += SFG_sincrec
                    energ_pos['ECrec-So'] = SFG_sincrec
                elif self.ani['Niv'] == 'I':
                    # Asignación de energía a tejido somático y reproducción
                    # de las vieiras juveniles con actividad reproductiva.
                    energ_pos['ECrec-So'] = SFG_sincrec * 0.55
                    self.ener['ECrec-So'] += energ_pos['ECrec-So']
                    if self.amb['Temp'][self.dias] < 26.0:
                        energ_pos['EPgon'] = SFG_sincrec*0.45
                        self.ener['EPgon'] += energ_pos['EPgon']
                    elif self.amb['Temp'][self.dias] >= 26.0:
                        if (self.dat['Mas-Go'] > 0) and \
                           (self.ener['EAgon'] <= self.gon_to_energ()):
                            self.ener['EAgon'] += SFG_sincrec * 0.45
                            self.ener['EMgon'] += SFG_sincrec * 0.45
                        else:
                            self.ener['ECrec-So'] += SFG_sincrec * 0.45
                            energ_pos['ECrec-So'] += SFG_sincrec * 0.45
                else:
                    # Asignación de energía a tejido somático y reproducción
                    # de las vieiras maduras.
                    energ_pos['ECrec-So'] = SFG_sincrec * 0.25
                    self.ener['ECrec-So'] += energ_pos['ECrec-So']
                    if self.amb['Temp'][self.dias] < 26.0:
                        energ_pos['EPgon'] = SFG_sincrec * 0.75
                        self.ener['EPgon'] += energ_pos['EPgon']
                    elif self.amb['Temp'][self.dias] >= 26.0:
                        if (self.dat['Mas-Go'] > 0) and \
                           (self.ener['EAgon'] <= self.gon_to_energ()):
                            self.ener['EAgon'] += SFG_sincrec * 0.75
                            self.ener['EMgon'] += SFG_sincrec * 0.75
                        else:
                            self.ener['ECrec-So'] += SFG_sincrec*0.75
                            energ_pos['ECrec-So'] += SFG_sincrec*0.75

                # Inversión de las ganancias.
                self.dat['Mas-Go'] += self.energ_to_gon(
                    energ_pos['EPgon'])
                self.dat['Mas-So'] += self.energ_to_soma(
                    energ_pos['ECrec-So'])
                self.dat['Mas-Sh'] += self.energ_to_shell(
                    energ_pos['ECrec-Sh'])
                self.dat['Tall'] += self.energ_to_Tshell(
                    energ_pos['ECrec-Sh'])

            elif SFG < 0:   # Animal en pérdidas.
                self.reg_evento('Pérdidas')
                # Si el animal entra en inanición.
                if dias_inan == 1 and SFG < 0:
                    self.dat['peso_org_inan'] = self.dat['Mas-So']
                    # Se guarda el peso antes de que entrará en inanición.

                # Calculo de perdidas
                self.ener['EPerd'] += SFG
                extra = 0
                perdida = 0
                peso_limite = self.dat['peso_org_inan'] * 0.6
                if self.dat['Mas-So'] > peso_limite:
                    # Si la masa somática es mayor al 60% del peso original,
                    # entonces el animal puede seguir perdiendo peso de sus
                    # reservas almacenadas en la masa somática.
                    perdida = self.soma_to_energ() + SFG
                    if perdida >= self.soma_to_energ(peso_limite):
                        self.dat['Mas-So'] = self.dat['Mas-So'] - \
                            self.energ_to_soma(-1 * SFG)
                        perdida = 0
                    else:
                        peso = self.dat['Mas-So'] - peso_limite
                        perdida = self.soma_to_energ(peso) + SFG
                        self.dat['Mas-So'] = peso_limite
                else:
                    perdida = SFG

                # Si el animal puede satisfacer su necesidad de energía con el
                # contenido de su gónada. (Para evitar morir)
                if perdida:
                    # verificar si la gónada puede satisfacer el gasto.
                    condicion = self.gon_to_energ() + self.ener[u'EAgon'] + \
                        perdida
                    if condicion >= 0:
                        # Los gastos se dividen 50/50, siempre dando prioridad
                        # a la gónada.
                        self.ener['EAgon'] = self.ener['EAgon'] + \
                            (perdida * 0.5)
                        if self.ener['EAgon'] < 0:
                            extra = self.ener['EAgon']
                            self.ener['EAgon'] = 0

                        # Nota: por la forma de programación, la energía de
                        # activación en la gónada solo puede ser menor o igual
                        # a la energía invertida en el tejido de la gónada.
                        self.dat['Mas-Go'] = self.dat['Mas-Go'] - \
                            self.energ_to_gon(-1 * ((perdida * 0.5) + extra))

                    else:
                        # Muerte si su peso disminuye por debajo de un 40% del
                        # peso original antes de entrar en inanición.
                        self.ani['Estado'] = 0
                        self.reg_evento('Muerte')
            # Fin del calculo de perdidas

            self.actualizar_nivel()
            self.dias += 1
            yield env.timeout(1)
            self.hist.append(deepcopy(self))

        return

    # ------------------------------------------------------------------
    def printopasto(self):
        """Funcíon "printopasto" Forma fácil de hacer la salida a CLI."""
        print(str(self))
        if self.even:
            print(self.even)
        elif self.even == []:
            print('\t\t [Sin Eventos]')
        return

    # ------------------------------------------------------------------
    def __str__(self):
        """Operador "__str__" ubica los datos en forma de texto simple."""
        variables = (
                self.nombre,
                str(self.ani['ID']),
                self.dias,
                self.dat['Tall'],
                self.Mas_total(),
                self.ani['Niv'],
                self.dat['Mas-So'],
                self.ener['ECrec-So'],
                self.ener['ECrec-Sh'],
                self.dat['Mas-Sh'],
                self.dat["Mas-Go"],
                self.ener['EPgon'],
                self.ener['EMgon'],
                self.ener['EPerd'],)
        return bivalvo_str % variables

    # ------------------------------------------------------------------
    def tabulado(self):
        """Función "tabulado" retorna en texto los datos para la tabla en GUI.

        Nota: debe seguir el orden de tabulado presentado en la variable
        "cabeceras_para_tablas" descrita en las constantes de la clase:
        Especie, Estado, Grup. Etario, Talla (dorso-ventral), Peso Total,
        Peso Concha, Peso Tejido, Peso Gónada, Energía Somat, Energía Conch,
        Energía P.Gon, Energía M.Gon, Energía Perd.
        """
        # Colocar el nombre del estado del animal (Legible para humanos)
        estado = "Vivo [1]"
        if not self.ani['Estado']:
            estado = "Muerto [0]"

        # Colocar el nombre del grupo etario del animal (Legible para humanos)
        etapa = next(
            clave for clave, valor in self.etapas_nomb.items()
            if valor == self.ani['Niv']
        )

        return (
                self.ani['ID'],
                self.nombre,
                estado,
                self.dias,
                etapa[3:],
                self.dat['Tall'],
                self.Mas_total(),
                self.dat['Mas-Sh'],
                self.dat['Mas-So'],
                self.dat['Mas-Go'],
                self.ener['ECrec-So'],
                self.ener['ECrec-Sh'],
                self.ener['EPgon'],
                self.ener['EAgon'],
                self.ener['EPerd'],
            )
        # El array contiene todos los datos a mostrar en la tabla

    # ------------------------------------------------------------------
    def graf_talla(self):
        """Función "graf_talla" retorna los valores de las tallas del animal.

        Crea los arreglos necesarios para ejecutar una gráfica de las tallas
        de la vieira durante toda la simulación.
        """
        # Define el titulo de la gráfica (Debe limitar el tamaño del texto)
        titulo = "Tallas dorso-ventral del\n " \
            "%s ID: %s" % (self.nombre, self.ani['ID'])
        tallas = list()  # tallas dorso-ventrales en centímetros.
        tiempo = list()  # día de simulación correspondiente.

        tallas.append(self.ini.dat["Tall"])
        tiempo.append(0)

        for i in self.hist:
            tallas.append(i.dat["Tall"])
            tiempo.append(i.dias)

        tallas.append(self.dat["Tall"])
        tiempo.append(self.dias)
        return (
            titulo,
            (
                tiempo,  # Eje X
                tallas,  # Eje Y
            ),
            "Día de la simulación",  # Leyenda de eje X
            "Talla dorso-ventral (cm)",  # Leyenda de eje Y
        )

    # ------------------------------------------------------------------
    def graf_peso_soma(self):
        """Función "graf_peso_soma" retorna los valores del peso somático.

        Crea los arreglos necesarios para ejecutar una gráfica del peso
        somático de la vieira durante toda la simulación.
        """
        # Define el titulo de la gráfica (Debe limitar el tamaño del texto)
        titulo = "Pesos Somáticos del\n " \
            "%s ID: %s" % (self.nombre, self.ani['ID'])
        soma = list()
        tiempo = list()  # día de simulación correspondiente.

        soma.append(self.ini.dat["Mas-So"])
        tiempo.append(0)

        for i in self.hist:
            soma.append(i.dat["Mas-So"])
            tiempo.append(i.dias)

        soma.append(self.dat["Mas-So"])
        tiempo.append(self.dias)
        return (
            titulo,
            (
                tiempo,  # Eje X
                soma,  # Eje Y
            ),
            "Día de la simulación",  # Leyenda de eje X
            "Peso Somático (gr)",  # Leyenda de eje Y
        )

    # ------------------------------------------------------------------
    def graf_peso_conc(self):
        """Función "graf_peso_conc" retorna los valores del peso de la concha.

        Crea los arreglos necesarios para ejecutar una gráfica del peso de la
        concha de la vieira durante toda la simulación.
        """
        # Define el titulo de la gráfica (Debe limitar el tamaño del texto)
        titulo = "Pesos de la concha del\n " \
            "%s ID: %s" % (self.nombre, self.ani['ID'])
        concha = list()
        tiempo = list()  # día de simulación correspondiente.

        concha.append(self.ini.dat["Mas-Sh"])
        tiempo.append(0)

        for i in self.hist:
            concha.append(i.dat["Mas-Sh"])
            tiempo.append(i.dias)

        concha.append(self.dat["Mas-Sh"])
        tiempo.append(self.dias)
        return (
            titulo,
            (
                tiempo,  # Eje X
                concha,  # Eje Y
            ),
            "Día de la simulación",  # Leyenda de eje X
            "Peso de la concha (gr)",  # Leyenda de eje Y
        )

    # ------------------------------------------------------------------
    def graf_peso_gona(self):
        """Función "graf_peso_gona" retorna los valores del peso de la gónada.

        Crea los arreglos necesarios para ejecutar una gráfica del peso de la
        gónada de la vieira durante toda la simulación.
        """
        # Define el titulo de la gráfica (Debe limitar el tamaño del texto)
        titulo = "Pesos de la gónada del\n " \
            "%s ID: %s" % (self.nombre, self.ani['ID'])
        gonada = list()
        tiempo = list()  # día de simulación correspondiente.

        gonada.append(self.ini.dat["Mas-Go"])
        tiempo.append(0)

        for i in self.hist:
            gonada.append(i.dat["Mas-Go"])
            tiempo.append(i.dias)

        gonada.append(self.dat["Mas-Go"])
        tiempo.append(self.dias)
        return (
            titulo,
            (
                tiempo,  # Eje X
                gonada,    # Eje Y
            ),
            "Día de la simulación",  # Leyenda de eje X
            "Peso de la gónada (gr)",  # Leyenda de eje Y
        )

    # ------------------------------------------------------------------
    def graf_ener_soma(self):
        """Función "graf_ener_soma" retorna la energía usada en el t. somático.

        Crea los arreglos necesarios para ejecutar una gráfica de la energía
        invertida en el tejido somático de la vieira durante toda la
        simulación.
        """
        # Define el titulo de la gráfica (Debe limitar el tamaño del texto)
        titulo = "Energía invertida en el tejido de la\n " \
            "%s ID: %s" % (self.nombre, self.ani['ID'])
        energ = list()
        tiempo = list()  # día de simulación correspondiente.

        energ.append(self.ini.ener["ECrec-So"])
        tiempo.append(0)
        for i in self.hist:
            energ.append(i.ener["ECrec-So"])
            tiempo.append(i.dias)

        energ.append(self.ener["ECrec-So"])
        tiempo.append(self.dias)
        return (
            titulo,
            (
                tiempo,  # Eje X
                energ,  # Eje Y
            ),
            "Día de la simulación",  # Leyenda de eje X
            "Energía (Joules)",  # Leyenda de eje Y
        )

    # ------------------------------------------------------------------
    def graf_ener_conc(self):
        """Función "graf_ener_conc" retorna la energía usada en la concha.

        Crea los arreglos necesarios para ejecutar una gráfica de la energía
        invertida en la estructura de la concha de la vieira durante toda la
        simulación.
        """
        # Define el titulo de la gráfica (Debe limitar el tamaño del texto)
        titulo = "Energía invertida en la concha del\n " \
            "%s ID: %s" % (self.nombre, self.ani['ID'])
        energ = list()
        tiempo = list()  # día de simulación correspondiente.

        energ.append(self.ini.ener["ECrec-Sh"])
        tiempo.append(0)
        for i in self.hist:
            energ.append(i.ener["ECrec-Sh"])
            tiempo.append(i.dias)

        energ.append(self.ener["ECrec-Sh"])
        tiempo.append(self.dias)
        return (
            titulo,
            (
                tiempo,  # Eje X
                energ,  # Eje Y
            ),
            "Día de la simulación",  # Leyenda de eje X
            "Energía (Joules)",  # Leyenda de eje Y
        )

    # ------------------------------------------------------------------
    def graf_ener_Pgona(self):
        """Función "graf_ener_Pgona" retorna la energía usada producir gónada.

        Crea los arreglos necesarios para ejecutar una gráfica de la energía
        invertida en la producción de tejido de la gónada de la vieira durante
        toda la simulación.
        """
        # Define el titulo de la gráfica (Debe limitar el tamaño del texto)
        titulo = "Energía para la gametogénesis del\n " \
            "%s ID: %s" % (self.nombre, self.ani['ID'])
        energ = list()
        tiempo = list()  # día de simulación correspondiente.

        energ.append(self.ini.ener["EPgon"])
        tiempo.append(0)
        for i in self.hist:
            energ.append(i.ener["EPgon"])
            tiempo.append(i.dias)

        energ.append(self.ener["EPgon"])
        tiempo.append(self.dias)
        return (
            titulo,
            (
                tiempo,  # Eje X
                energ,  # Eje Y
            ),
            "Día de la simulación",  # Leyenda de eje X
            "Energía (Joules)",  # Leyenda de eje Y
        )

    # ------------------------------------------------------------------
    def graf_ener_Agona(self):
        """Función "graf_ener_Agona" retorna energía usada madurar la gónada.

        Crea los arreglos necesarios para ejecutar una gráfica de la energía
        invertida en la maduración de tejido de la gónada de la vieira durante
        toda la simulación.
        """
        # Define el titulo de la gráfica (Debe limitar el tamaño del texto)
        titulo = "Energía para la maduración de la\n " \
            "gónada: %s ID: %s" % (self.nombre, self.ani['ID'])
        energ = list()
        tiempo = list()  # día de simulación correspondiente.

        energ.append(self.ini.ener["EAgon"])
        tiempo.append(0)
        for i in self.hist:
            energ.append(i.ener["EAgon"])
            tiempo.append(i.dias)

        energ.append(self.ener["EAgon"])
        tiempo.append(self.dias)
        return (
            titulo,
            (
                tiempo,  # Eje X
                energ,  # Eje Y
            ),
            "Día de la simulación",  # Leyenda de eje X
            "Energía (Joules)",  # Leyenda de eje Y
        )

    # ------------------------------------------------------------------
    def graf_ener_perd(self):
        """Función "graf_ener_perd" retorna las perdidas de energía.

        Crea los arreglos necesarios para ejecutar una gráfica de las perdidas
        de reservas de energía de la vieira durante toda la simulación.
        """
        # Define el titulo de la gráfica (Debe limitar el tamaño del texto)
        titulo = "Energía perdida durante la simulación del\n " \
            "%s ID: %s" % (self.nombre, self.ani['ID'])
        energ = list()
        tiempo = list()  # día de simulación correspondiente.

        energ.append(self.ini.ener["EPerd"])
        tiempo.append(0)
        for i in self.hist:
            energ.append(-1 * i.ener["EPerd"])
            tiempo.append(i.dias)

        energ.append(-1 * self.ener["EPerd"])
        tiempo.append(self.dias)
        return (
            titulo,
            (
                tiempo,  # Eje X
                energ,  # Eje Y
            ),
            "Día de la simulación",  # Leyenda de eje X
            "Energía Perdida (Joules)",  # Leyenda de eje Y
        )

    # ------------------------------------------------------------------
# ----------------------------------------------------------------------
