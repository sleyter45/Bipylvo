"""
Dialogo para mostrar las instrucciones y/o mensajes de ayudas al usuario.

Este documento define la ventana y sus funciones para mostrar diálogos de ayuda
para los usuarios, basados en textos redactados para cada función.
"""
# ----------------------------------------------------------------------


# --------------------------------Libs----------------------------------
from __future__ import unicode_literals
# ----------------------------------------------------------------------
import sys

from os import path
# ----------------------------------------------------------------------
from PyQt5.QtCore import QFile, QSettings, QSize, QStandardPaths, \
                         QTextCodec, Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDesktopWidget, QDialog, QPushButton, \
                            QSizePolicy, QTextBrowser, QVBoxLayout
# ----------------------------------------------------------------------
from markdown import Markdown
# ----------------------------------------------------------------------


# -----------------------------DialogWind--------------------------------
class ayudasWind(QDialog):
    """Clase "ayudasWind" contiene definición de la ventana de ayuda."""

    # ------------------------------------------------------------------
    def __init__(self, titulo, msj=None, parent=None):
        """Constructor de "ayudasWind" que muestra las ayudas en texto.

        Parámetros:
            - titulo: string con el texto que titula la ventana.
            - msj: string con la dirección del archivo a mostrar o una lista
                de direcciones.
            - parent: padre de la ventana.
        """
        super(ayudasWind, self).__init__(parent)
        self.direc = parent.direcc
        # Colocar el titulo de la ventana
        self.setWindowTitle("Bipylvo - %s" % str(titulo))
        self.setWindowModality(Qt.NonModal)
        self.setMinimumSize(QSize(600, 500))
        self.setAttribute(Qt.WA_DeleteOnClose)
        # Cargar preferencias del usuario:
        usrdir = QStandardPaths.standardLocations(
            QStandardPaths.ConfigLocation)
        if isinstance(usrdir, list):
            usrdir = usrdir[0]
        self.opciones = QSettings(
            path.join(usrdir, 'bipylvo/bipylvo_opciones.ini'),
            QSettings.IniFormat,
        )
        # Definir los widgets
        self.textWidget = QTextBrowser(self)
        self.textWidget.setAlignment(Qt.AlignCenter | Qt.AlignVCenter)

        fallB = "Recursos/mdIcons/ic_highlight_off_black_48dp.png"
        fallB = path.join(parent.direcc, fallB)
        self.cerrarButton = QPushButton(
            QIcon.fromTheme("window-close", QIcon(fallB)),
            "Cerrar",
            self,
        )
        # Acciones para cada boton.
        self.cerrarButton.clicked.connect(self.close)
        # Definir el widget para ver el texto
        direc = False
        if isinstance(msj, str):
            direc = path.join(parent.direcc, msj)
        elif isinstance(msj, list):
            direc = list()
            for archivo in msj:
                direc.append(path.join(parent.direcc, archivo))
        self.abrirArch(direc)
        # Definir el Layout
        self.rejilla = QVBoxLayout()
        # Agregar widgets a la lista.
        self.rejilla.addWidget(self.textWidget)
        self.rejilla.addWidget(self.cerrarButton)
        self.setLayout(self.rejilla)
        # Ajustar las políticas de las widgets
        self.textWidget.setSizePolicy(
            QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.cerrarButton.setSizePolicy(
            QSizePolicy.Expanding, QSizePolicy.Fixed)

        self.centrar()
        # Creación de la clase finalizada, para mostrar el dialogo debe
        # ejecutar ".exec_()"
        return

    # ------------------------------------------------------------------
    def abrirArch(self, arch=None):
        """Función "ayudasWind.abrirArch" abre y lee los datos de un archivo.

        Parámetros: (self, arch=None)
            - arch: dirección del archivo dentro del paquete de bipylvo, o
                lista de direcciones a usar.
        """
        if isinstance(arch, str):
            archi = QFile(arch)
            if not QFile.exists(arch):
                msj = "Alerta: El archivo [%s] no existe." % arch
                self.textWidget.setPlainText(msj)
                sys.stdout.write(msj)
                return

            if not archi.open(QFile.ReadOnly):
                msj = "Alerta: El archivo [%s] no es legible." % arch
                self.textWidget.setPlainText(msj)
                sys.stdout.write(msj)
                return

            data = archi.readAll()
            codec = QTextCodec.codecForName("UTF-8")
            unistr = codec.toUnicode(data)

            html = Markdown(
                extensions=['markdown.extensions.tables'],
            ).convert(unistr)
            ico = "Recursos/ico"
            ico = path.join(self.direc, ico)
            self.textWidget.setSearchPaths([
                ico,
            ])
            self.textWidget.setHtml(html)
            self.textWidget.setOpenExternalLinks(True)
        elif isinstance(arch, list):
            datos = str()
            for archivo in arch:
                archi = QFile(archivo)
                if not QFile.exists(archivo):
                    msj = "Alerta: El archivo [%s] no existe." % archivo
                    self.textWidget.setPlainText(msj)
                    sys.stdout.write(msj)
                    return

                if not archi.open(QFile.ReadOnly):
                    msj = "Alerta: El archivo [%s] no es legible." % archivo
                    self.textWidget.setPlainText(msj)
                    sys.stdout.write(msj)
                    return

                data = archi.readAll()
                codec = QTextCodec.codecForName("UTF-8")
                unistr = codec.toUnicode(data)
                datos = datos + unistr

            html = Markdown(
                extensions=['markdown.extensions.tables'],
            ).convert(datos)
            ico = "Recursos/ico"
            ico = path.join(self.direc, ico)
            self.textWidget.setSearchPaths([
                ico,
            ])
            self.textWidget.setHtml(html)
            self.textWidget.setOpenExternalLinks(True)
        else:
            msj = "Alerta: El mensaje a mostrar al usuario no fue definido."
            self.textWidget.setPlainText(msj)
            sys.stdout.write(msj)
        return

    # ------------------------------------------------------------------
    def centrar(self):
        """Función "ayudasWind.centrar".

        Ubicar la aplicación en el medio de la pantalla.
        Parámetros: (self)
        """
        pantalla = QDesktopWidget().screenGeometry()
        tam = self.geometry()
        self.move(
            (pantalla.width()-tam.width())/2,
            (pantalla.height()-tam.height())/2,
        )
        return

    # ------------------------------------------------------------------
    # ------------------------------------------------------------------
# ----------------------------------------------------------------------
