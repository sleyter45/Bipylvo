#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Archivo "agregarTempUI.py" define el dialogo para agregar temperaturas.

Aquí se define la clase "AgregarDialog" cuya función es permitir al usuario
agregar temperaturas en la lista de temperaturas de los parámetros del ambiente
a simular de manera implícita, es decir, este dialogo modifica las variable
"argsim" que contiene las condiciones del escenario de simulación, incluyendo
la lista de temperaturas a simular. Pero para crear una instancia del dialogo
se necesitan 2 parámetros obligatorios:
    "especie" = es la clase o especie a simular.
     "parent" = representa la ventana (objeto) padre de esta ventana.
"""
# ---------------------------------Libs----------------------------------
from __future__ import unicode_literals

import sys

from os import path

from PyQt5.QtCore import QSize, Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDesktopWidget, QDialog, QDoubleSpinBox, \
    QGridLayout, QHBoxLayout, QLabel, QPushButton, QSizePolicy,\
    QToolButton
# ----------------------------------------------------------------------


# ------------------------Ventana-de-Dialogo----------------------------
class AgregarDialog(QDialog):
    """Clase "AgregarDialog" implementa el dialogo para agregar temperaturas.

    La clase hereda de "QDialog" de PyQt5 para crear un dialogo desplegable
    que permite al usuario agregar temperaturas en la lista de temperaturas de
    los parámetros del ambiente a simular. Esta clase requiere 2 parámetros
    obligatorios:
        "especie" = es la clase o especie a simular, la cual determina los
            limites de temperatura a usar.
         "parent" = representa la ventana (objeto) padre de esta ventana.
    """

    # ------------------------------------------------------------------
    def __init__(self, especie, parent):
        """Constructor del dialogo desplegable "AgregarDialog".

        La clase hereda de "QDialog" de PyQt5 para crear un dialogo desplegable
        que permite al usuario agregar temperaturas en la lista de temperaturas
        de los parámetros del ambiente a simular. Esta clase requiere 2
        parámetros obligatorios:
            "especie" = es la clase o especie a simular, la cual determina los
                limites de temperatura a usar.
             "parent" = representa la ventana (objeto) padre de esta ventana.
        """
        # cargar el constructor de la clase padre.
        super(AgregarDialog, self).__init__(parent)
        self.setAttribute(Qt.WA_DeleteOnClose)
        # Colocar el titulo de la ventana
        self.setWindowTitle("Bipylvo - Agregar temperatura.")
        self.setWindowModality(Qt.WindowModal)
        self.setMinimumSize(QSize(440, 115))
        # Comprobar que exista una especie definida.
        self.error = 0
        if not especie:
            texto = "Error. Debe elegir una especie a simular previamente."
            desc = "Debe elegir una especie de bivalvo para poder agregar" \
                " nuevas temperaturas a la lista."
            sys.stderr.write(texto)
            parent.error_msj(texto, descrip=desc)
            self.close()
            self.error = 1
            return
        # guardar padre:
        self.padre = parent
        # Definir los Layout
        self.rejilla = QGridLayout()
        self.buttomsLayout = QHBoxLayout()
        # Definir los widgets
        self.temperaturaLabel = QLabel("Temperatura a agregar (°C):", self)
        self.temperaturaLabel.setAlignment(Qt.AlignCenter | Qt.AlignVCenter)
        # Spinbox
        self.tempDoubleSpinBox = QDoubleSpinBox(parent=self)
        self.tempDoubleSpinBox.setAlignment(Qt.AlignCenter)
        self.tempDoubleSpinBox.setMinimum(especie.limites['tempMin'])
        self.tempDoubleSpinBox.setMaximum(especie.limites['tempMax'])
        self.tempDoubleSpinBox.setSizePolicy(
            QSizePolicy.Expanding,
            QSizePolicy.Fixed,
        )
        # Boton de ayuda al usuario
        fallB = "Recursos/mdIcons/ic_help_outline_black_48dp.png"
        fallB = path.join(parent.direcc, fallB)
        self.tempToolButton = QToolButton(self)
        self.tempToolButton.setIcon(
            QIcon.fromTheme("help-contents", QIcon(fallB)),
        )
        # Botones finales
        fallB = "Recursos/mdIcons/ic_input_black_48dp.png"
        fallB = path.join(parent.direcc, fallB)
        self.agregarButton = QPushButton(
            QIcon.fromTheme("list-add", QIcon(fallB)),
            "Agregar",
            self,
        )
        self.agregarButton.setSizePolicy(
            QSizePolicy.Expanding,
            QSizePolicy.Fixed,
        )
        fallB = "Recursos/mdIcons/ic_add_circle_outline_black_48dp.png"
        fallB = path.join(parent.direcc, fallB)
        self.continuarButton = QPushButton(
            QIcon.fromTheme("go-next", QIcon(fallB)),
            "Agregar y Continuar",
            self,
        )
        self.continuarButton.setSizePolicy(
            QSizePolicy.Expanding,
            QSizePolicy.Fixed,
        )
        fallB = "Recursos/mdIcons/ic_delete_black_48dp.png"
        fallB = path.join(parent.direcc, fallB)
        self.descartarButton = QPushButton(
            QIcon.fromTheme("user-trash", QIcon(fallB)),
            "Descartar",
            self,
        )
        self.descartarButton.setSizePolicy(
            QSizePolicy.Expanding,
            QSizePolicy.Fixed,
        )
        # Agregar widgets a los layouts
        self.buttomsLayout.addWidget(self.agregarButton)
        self.buttomsLayout.addWidget(self.continuarButton)
        self.buttomsLayout.addWidget(self.descartarButton)
        # layout de rejilla requiere posiciones
        self.rejilla.addWidget(self.temperaturaLabel, 0, 0, 1, 2)
        self.rejilla.addWidget(self.tempDoubleSpinBox, 1, 0)
        self.rejilla.addWidget(self.tempToolButton, 1, 1)
        self.rejilla.addLayout(self.buttomsLayout, 2, 0, 1, 2)
        # Agregar rejilla al dialogo
        self.setLayout(self.rejilla)
        # conectar eventos de botones.
        self.agregarButton.clicked.connect(self.agregar)
        self.continuarButton.clicked.connect(self.continuar)
        self.descartarButton.clicked.connect(self.close)
        # Creación de la clase finalizada, para mostrar el dialogo debe
        # ejecutar ".exec_()"
        return

    # ------------------------------------------------------------------
    def centrar(self):
        """Función "centrar" ubica la ventana en el medio de la pantalla."""
        pantalla = QDesktopWidget().screenGeometry()
        tam = self.geometry()
        self.move(
                    (pantalla.width()-tam.width())/2,
                    (pantalla.height()-tam.height())/2,
        )
        return

    # ------------------------------------------------------------------
    def agregar(self):
        """Función "agregar" agrega de temperatura a la lista y cierra."""
        # obtener dato de la temperatura:
        valor = self.tempDoubleSpinBox.value()
        # agregar a la lista de temperaturas al final:
        self.padre.argsim["temps"].append(valor)
        self.done(0)
        return

    # ------------------------------------------------------------------
    def continuar(self):
        """Función "continuar" agrega de temperatura a la lista y no cierra."""
        # obtener dato de la temperatura:
        valor = self.tempDoubleSpinBox.value()
        # agregar a la lista de temperaturas al final:
        self.padre.argsim["temps"].append(valor)
        # actualizar lista de temperaturas en la gui
        self.padre.actualizar_temps()
        return

    # ------------------------------------------------------------------
    # ------------------------------------------------------------------
# ----------------------------------------------------------------------
