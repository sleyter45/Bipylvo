#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Archivo "graficasUI.py" define la ventana de dialogo PyQt5 para gráficas.

Aquí se define la clase "GraficosDialog" cuya función es crear y mostrar un
dialogo con la capacidad de dibujar gráficas usando Matplotlib y QT5.
"""
# ---------------------------------Libs----------------------------------
from __future__ import unicode_literals

import sys

from os import path

from PyQt5.QtCore import QSettings, QSize, QStandardPaths, Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDesktopWidget, QDialog, QLabel, QMessageBox, \
                            QPushButton, QSizePolicy, QVBoxLayout

import matplotlib
# --------------------------Definir-Backends-----------------------------
matplotlib.use('Qt5Agg')  # Hacer obligatorio el uso del backend para QT5.
# -----------------------------------------------------------------------
# Se debe ignorar los errores de importaciones E402
# (module level import not at top of file)
from matplotlib import figure  # noqa: E402
from matplotlib.ticker import FormatStrFormatter  # noqa: E402
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as \
    FigureCanvas  # noqa: E402
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as \
    NavigationToolbar  # noqa: E402
# ----------------------------------------------------------------------


# -------------------------------Lienzo---------------------------------
class LienzoWidget(FigureCanvas):
    """Clase "LienzoWidget" crea un QWidget para colocar una gráfica."""

    # ------------------------------------------------------------------
    def __init__(
        self, titulo, datos, leyenda_x, leyenda_y,
        width=16, height=9, dpi=96, parent=None,
    ):
        """Constructor de "LienzoWidget" que crea el lienzo de la gráfica.

        Para dibujar la gráfica se requiere obligatoriamente:
            - Un titulo en formato de texto.
            - Un conjunto de datos para dibujar cada punto X,Y en el plano
            cartesiano. Los "datos" deben estar en formato lista, y dicha lista
            debe contener dos (2) o tres (3) listas de números. El primer
            conjunto de números representa el eje X, el segundo el eje Y, y el
            tercero es opcional y se usa para colocar varianzas o margen de
            error en el eje Y.
            - Una leyenda para el eje X (texto).
            - Una leyenda para el eje Y (texto).
        """
        # guardar los datos a usar.
        self.titulo = str(titulo)
        self.datos = datos
        self.leyenda_x = leyenda_x
        self.leyenda_y = leyenda_y
        # inicia creando la gráfica.
        self.figura = figure.Figure(
            figsize=(width, height),
            dpi=dpi,
            tight_layout=True,
        )
        # crea los ejes en la gráfica nueva.
        self.vertices = self.figura.add_subplot(111)

        # Iniciar constructor del padre:
        super(LienzoWidget, self).__init__(self.figura)
        # colocar widget padre.
        self.setParent(parent)
        # redimencionamiento y talla
        self.setSizePolicy(
            QSizePolicy.Expanding,
            QSizePolicy.Expanding,
        )
        self.updateGeometry()
        # Dibujar la gráfica.
        self.evento_graficar()
        return

    # ------------------------------------------------------------------
    def evento_graficar(self):
        """Función "evento_gradicar" dibuja la gráfica.

        Dibuja la gráfica asignada usando los datos recibidos anteriormente.
        No recibe argumentos.
        """
        # Descartar datos de gráfica inicial:
        self.vertices.cla()
        # Rejilla!
        self.vertices.grid(True)
        # Cuadrar cantidad de marcas en la gráfica
        largo = len(self.datos[0])
        marcas_cada = 1
        if largo >= 20:
            marcas_cada = int(largo * 0.05)
        # Cargar datos del usuario contenidos en self.datos:
        if len(self.datos) == 2:
            self.vertices.plot(
                self.datos[0],
                self.datos[1],
                "o-",
                markevery=marcas_cada,
                markersize=4,
            )
        elif len(self.datos) == 3:
            self.vertices.errorbar(
                self.datos[0],
                self.datos[1],
                yerr=self.datos[2],
                fmt="o-",
                markevery=marcas_cada,
                markersize=4,
            )
        else:
            texto = "Error al gráficar: los datos no cumplen " \
                "con el formato de lista correcto."
            # Alertar sobre errores
            sys.stderr.write(texto)
            alerta = QMessageBox(self.parentWidget())
            alerta.setIcon(QMessageBox.Warning)
            alerta.setWindowTitle("Bipylvo - Gráfica - Alerta de Error")
            alerta.setText(texto)
            alerta.setWindowModality(Qt.WindowModal)
            alerta.setMinimumWidth(320)
            alerta.setStandardButtons(QMessageBox.Close)
            alerta.button(QMessageBox.Close).setText("Cerrar Alerta")
            alerta.setDefaultButton(QMessageBox.Close)
            alerta.exec_()
            self.parent().close()
            return

        # Leyendas:
        self.vertices.set_title(self.titulo)
        self.vertices.set_xlabel(self.leyenda_x)
        self.vertices.set_ylabel(self.leyenda_y)
        self.vertices.yaxis.set_major_formatter(FormatStrFormatter('%.4f'))
        # Dibujar gráfica.
        self.draw()
        return

    # ------------------------------------------------------------------
# ----------------------------------------------------------------------


# ------------------------Ventana-de-Dialogo----------------------------
class GraficosDialog(QDialog):
    """Clase "GraficosDialog" define el QDialog dedicado para gráficas.

    Esta clase es una implementación de Matplotlib y PyQt5 para crear
    gráficas basadas en los datos obtenidos en el simulador.
    """

    # ------------------------------------------------------------------
    def __init__(self, titulo, datos, leyenda_x, leyenda_y, parent):
        """Constructor del Dialogo "GraficosDialog".

        Clase que hereda las propiedades de QDialog de QT, y hace uso de la
        integración con la librería Matplotlib para generar gráficas con los
        siguientes parámetros obligatorios:
            - Un titulo en formato de texto.
            - Un conjunto de datos para dibujar cada punto X,Y en el plano
            cartesiano. Los "datos" deben estar en formato lista, y dicha lista
            debe contener dos (2) o tres (3) listas de números. El primer
            conjunto de números representa el eje X, el segundo el eje Y, y el
            tercero es opcional y se usa para colocar varianzas o margen de
            error en el eje Y.
            - Una leyenda para el eje X (texto).
            - Una leyenda para el eje Y (texto).
            - Un objeto QWidget que representa al la ventana padre.
        """
        # cargar el constructor de la clase padre.
        super(GraficosDialog, self).__init__(parent)
        self.setAttribute(Qt.WA_DeleteOnClose)
        # Cargar preferencias del usuario:
        usrdir = QStandardPaths.standardLocations(
            QStandardPaths.ConfigLocation)
        if isinstance(usrdir, list):
            usrdir = usrdir[0]
        self.opciones = QSettings(
            path.join(usrdir, 'bipylvo/bipylvo_opciones.ini'),
            QSettings.IniFormat,
        )
        self.opciones.setFallbacksEnabled(False)
        # Colocar default dir para matplotlib.
        temp = QStandardPaths.standardLocations(
            QStandardPaths.DocumentsLocation)
        if isinstance(temp, list):
            temp = temp[0]
        matplotlib.rcParams["savefig.directory"] = temp
        # Colocar el titulo de la ventana
        self.setWindowTitle("Bipylvo - Gráfica: %s" % str(titulo))
        self.setWindowModality(Qt.NonModal)
        self.setMinimumSize(QSize(500, 400))
        # Definir el Layout
        self.rejilla = QVBoxLayout()
        # Definir los widgets
        self.ctl_label = QLabel("Controles de la gráfica:", self)
        self.ctl_label.setAlignment(Qt.AlignCenter | Qt.AlignVCenter)
        # Define la instancia de la gráfica de matplotlib.
        self.plano = LienzoWidget(
            titulo,
            datos,
            leyenda_x,
            leyenda_y,
            parent=self,
        )
        self.controles = NavigationToolbar(self.plano, self)
        # botón de cerrar el dialogo
        fallB = "Recursos/mdIcons/ic_highlight_off_black_48dp.png"
        fallB = path.join(parent.direcc, fallB)
        self.cerrarButton = QPushButton(
            QIcon.fromTheme("window-close", QIcon(fallB)),
            "Cerrar",
            self,
        )
        # Acciones para cada boton.
        self.cerrarButton.clicked.connect(self.close)
        # Implementar la rejilla
        self.rejilla.addWidget(self.plano)
        self.rejilla.addWidget(self.ctl_label)
        self.rejilla.addWidget(self.controles)
        self.rejilla.addWidget(self.cerrarButton)
        self.setLayout(self.rejilla)
        # Ajustar las políticas de las widgets
        self.ctl_label.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.controles.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.cerrarButton.setSizePolicy(
            QSizePolicy.Expanding, QSizePolicy.Fixed)
        # Creación de la clase finalizada, para mostrar el dialogo debe
        # ejecutar ".exec_()"
        return

    # ------------------------------------------------------------------
    def centrar(self):
        """Función "centrar" ubica la ventana en el medio de la pantalla."""
        pantalla = QDesktopWidget().screenGeometry()
        tam = self.geometry()
        self.move(
                    (pantalla.width()-tam.width())/2,
                    (pantalla.height()-tam.height())/2,
        )
        return

    # ------------------------------------------------------------------
    def resizeEvent(self, evento):
        """Función "Grafica.resizeEvent".

        Maneja el evento de redimensión de la ventana del dialogo permitiendo
        ajustar la geometría y el tamaño del plano de la gráfica al tamaño de
        la ventana.
        Parámetros: (self, evento)
        """
        self.plano.updateGeometry()
        self.plano.figura.canvas.update()
        self.plano.figura.tight_layout()
        evento.accept()
        return

    # ------------------------------------------------------------------
    def showEvent(self, evento):
        """Función "Grafica.showEvent".

        Maneja el evento mostrar de la ventana del dialogo permitiendo
        ajustar la geometría y el tamaño del plano de la gráfica al tamaño de
        la ventana.
        Parámetros: (self, evento)
        """
        super(GraficosDialog, self).showEvent(evento)
        self.plano.updateGeometry()
        self.plano.figura.canvas.update()
        self.plano.figura.tight_layout()
        evento.accept()
        return
# ----------------------------------------------------------------------
