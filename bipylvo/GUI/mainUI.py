#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Archivo "mainUI.py".

Define la clase MainWind y sus componentes, desarrollada para PyQt5 con la
finalidad de ejecutar una simulación usando la librería SimPy, donde los
comportamientos están definidos como objetos en la carpeta Especies y deben
ser importados en este archivo para colocarlos a disposición del usuario.

Las importaciones de las Especies disponibles se realizan en:
    "from Especies.Nombre_del_Archivo import Clase"
Y debe agregar la "Clase" a la variable "módulos" creando una nueva linea
cumpliendo el formato de un diccionario en Python y usando el nombre asignado
a la clase por su creador en la variable "nombre":
    CLASE.nombre: CLASE,      <-- Dentro del diccionario
"""
# --------------------------------Libs----------------------------------
from __future__ import unicode_literals
# ----------------------------------------------------------------------
import sys

from os import mkdir, path
# ------------------------------Especies--------------------------------
# Se debe importar cada modulo que desea poder simular.
from Especies.eziczac import Eziczac_tropical
# from Especies.ARCHIVO import CLASE
# ----------------------------------------------------------------------
from GUI.agreagarTempUI import AgregarDialog
from GUI.ayudasUI import ayudasWind
from GUI.graficasUI import GraficosDialog

from LIB.estads import promediosYvarianzas
from LIB.tempxtiemp import temperaturas_para_el_tiempo_dic
# ----------------------------------------------------------------------
from PyQt5 import uic
from PyQt5.QtCore import QSettings, QSize, QStandardPaths, Qt
from PyQt5.QtGui import QIcon, QStandardItem, QStandardItemModel
from PyQt5.QtWidgets import QAction, QDesktopWidget, QHeaderView, \
                            QListWidgetItem, QMainWindow, QMessageBox, \
                            QProgressDialog
# ----------------------------------------------------------------------
from numpy import ndarray
# ----------------------------------------------------------------------
import simpy
# ----------------------------------------------------------------------
# -------------------------Refistro-Especies----------------------------
# Se debe registrar aquí cada modulo para simular, siguiendo la sintaxis
# de un diccionario en Python.
modulos = {
    Eziczac_tropical.nombre: Eziczac_tropical,
    # CLASE.nombre: CLASE,
}
# ----------------------------------------------------------------------


# ------------------------------MainWind--------------------------------
class MainWind(QMainWindow):
    """Clase "MainWind" define la ventana principal y sus funciones."""

    # ------------------------------------------------------------------
    def __init__(self, parent=None, direc='.'):
        """Contructor __init__ para "MainWind".

        Inicialíza la clase de la ventana principal del simulador Bipylvo
        para la interfaz gráfica diseñada en PyQt5, Simpy y Numpy.
        Recibe una ventana padre (parent=None), y una carpeta donde se
        ejecuta la aplicación (direc=".").

        Parámetros: (self, parent=None, direc=".")
        """
        self.direcc = direc
        # Cargar el constructor padre de la ventana.
        super(MainWind, self).__init__(parent)
        # Buscar y comprobar que el directorio existe.
        try:  # Tratar de crear el directorio bipylvo.
            usrdir = QStandardPaths.standardLocations(
                QStandardPaths.ConfigLocation)
            if isinstance(usrdir, list):
                usrdir = usrdir[0]
            mkdir(path.join(usrdir, 'bipylvo'), 0o775)
        except FileExistsError as err:
            pass  # En caso de ya existir la carpeta no se modifica
        except FileNotFoundError as err:
            self.error_msj(
                str(err),
                descrip="No se puede acceder al directorio: %s" % usrdir,
            )
        except Exception as err:
            texto = "Error en Directorio %s." % path.join(usrdir, 'bipylvo')
            sys.stdout.write(texto)
            sys.stdout.write("%s \n" % str(err))
            self.error_msj(texto, descrip=str(err))
            sys.exit(str(err))
        # Cargar el esquema de la ventana hecho en Qt Designer.
        uic.loadUi(path.join(direc, 'UI/bipylvo.ui'), self)
        # Cargar preferencias del usuario:
        self.opciones = QSettings(
            path.join(usrdir, 'bipylvo/bipylvo_opciones.ini'),
            QSettings.IniFormat,
        )
        self.opciones.setFallbacksEnabled(False)
        # Ubicar la ventana.
        self.centrar()
        # Arreglos de datos para organizar los parámetros de la simulación.
        self.argsim = {
                        'esp': None,
                        'grup': None,
                        'alim': None,
                        'tiemp': None,
                        'cant': None,
                        'temps': list(),
                        }
        # Modificar la apariencia de la app:
        temp = 'Recursos/ico/bipylvo-logox64.png'
        self.setWindowIcon(QIcon(path.join(direc, temp)))
        # Barra de menú:
        # Menú principal, botón de Cerrar
        fallB = 'Recursos/mdIcons/ic_highlight_off_black_48dp.png'
        fallB = path.join(self.direcc, fallB)
        self.salirMenuApp = QAction(
            QIcon.fromTheme('window-close', QIcon(fallB)), '&Cerrar', self,
        )
        self.salirMenuApp.setShortcut('Ctrl+Q')
        self.salirMenuApp.setStatusTip('Cerrar la aplicación.')
        self.salirMenuApp.triggered.connect(self.close)

        self.initAyudaMenuApp = QAction(
            'Ayudas al &Inicio de la aplicación', parent=self, checkable=True,
        )
        self.initAyudaMenuApp.setShortcut('Ctrl+I')
        if self.opciones.value('Helps') == '1':
            self.initAyudaMenuApp.setChecked(True)
        self.initAyudaMenuApp.triggered.connect(self.ayudasCheckMenuButton)
        # Menú de ayuda, botón de Como Usar
        fallB = 'Recursos/mdIcons/ic_help_outline_black_48dp.png'
        fallB = path.join(self.direcc, fallB)
        self.usoMenuAyuda = QAction(
            QIcon.fromTheme('help-contents', QIcon(fallB)),
            'Como &Usar el Simulador', self,
        )
        self.usoMenuAyuda.setShortcut('F1')
        self.usoMenuAyuda.setStatusTip(
            'Descripción de como usar el simulador.')
        self.usoMenuAyuda.triggered.connect(self.ayudaUso)
        # Menú de ayuda, botón de Información
        fallB = 'Recursos/mdIcons/ic_info_outline_black_48dp.png'
        fallB = path.join(self.direcc, fallB)
        self.descMenuAyuda = QAction(
            QIcon.fromTheme('help-contents', QIcon(fallB)),
            'Acerca de &Bipylvo', self,
        )
        self.descMenuAyuda.setShortcut('Ctrl+H')
        self.descMenuAyuda.setStatusTip('Descripción del simulador.')
        self.descMenuAyuda.triggered.connect(self.acercaDeBipylvo)
        # Integrar menú y botones
        self.menubar = self.menuBar()
        # Menú de la aplicación.
        self.menuApp = self.menubar.addMenu('&Aplicación')
        self.menuApp.addAction(self.initAyudaMenuApp)
        self.menuApp.addSeparator()
        self.menuApp.addAction(self.salirMenuApp)
        # Menú de ayuda.
        self.menuAyuda = self.menubar.addMenu('A&yuda')
        self.menuAyuda.addAction(self.usoMenuAyuda)
        self.menuAyuda.addAction(self.descMenuAyuda)

        # Iconos en los botones:
        fallB = 'Recursos/mdIcons/ic_playlist_add_black_48dp.png'
        fallB = path.join(direc, fallB)
        self.addTempButton.setIcon(
            QIcon.fromTheme('list-add', QIcon(fallB)),
        )
        fallB = 'Recursos/mdIcons/ic_delete_forever_black_48dp.png'
        fallB = path.join(direc, fallB)
        self.rmTempButton.setIcon(
            QIcon.fromTheme('list-remove', QIcon(fallB)),
        )
        fallB = 'Recursos/mdIcons/ic_play_circle_outline_black_48dp.png'
        fallB = path.join(direc, fallB)
        self.simularButton.setIcon(
            QIcon.fromTheme('go-next', QIcon(fallB)),
        )
        fallB = 'Recursos/mdIcons/ic_open_in_new_black_48dp.png'
        fallB = path.join(direc, fallB)
        self.graficarButton.setIcon(
            QIcon.fromTheme('window-new', QIcon(fallB)),
        )
        # Agrega los Datos a mostrar en el combobox
        # de Especies y conecta el evento.
        self.especies = ['Seleccione la Especie', ]+list(modulos.keys())
        self.especieComboBox.addItems(self.especies)
        self.especieComboBox.currentIndexChanged.connect(self.cambios_especie)
        # Evita que el usuario busque datos en el
        # combobox de Grupos y conecta el evento.
        self.grupoetarioComboBox.setEnabled(False)
        self.grupoetarioComboBox.currentIndexChanged.connect(
            self.cambios_grupoetario)
        # Evita que el usuario lanzé una simulación
        # si los parámetros no estan colocados.
        self.simularButton.setEnabled(False)
        # Agrega los disparadores de eventos.
        self.simularButton.clicked.connect(self.simular)
        self.simularButton.setAutoDefault(True)
        self.addTempButton.clicked.connect(self.agregar_temp)
        self.rmTempButton.clicked.connect(self.remover_temp)
        # Banderas:
        self.tiene_actividad = False  # guarda si el usuario tiene datos en uso
        self.detallesid_flag = True
        self.detallesdia_flag = True
        self.graficasbox_flag = True
        # Agregar eventos para ver datos en las tablas y detalles.
        self.simulTableView.clicked.connect(self.evento_click_tabla)
        self.graficarButton.clicked.connect(self.evento_grafica_de_datos)
        # Eventos al presionar "Cambiar los datos"
        self.detalles_idSpinBox.valueChanged.connect(
            self.evento_detalles_udp_clicked,
        )
        self.detalles_diaSpinBox.valueChanged.connect(
            self.evento_detalles_udp_clicked,
        )
        self.graficarComboBox.currentIndexChanged.connect(
            self.evento_grafica_de_datos,
        )
        # Evita que el usuario ejecute una búsqueda o gráfica sin tener datos.
        self.graficarComboBox.setEnabled(False)
        self.graficarButton.setEnabled(False)
        self.detalles_idSpinBox.setEnabled(False)
        self.detalles_diaSpinBox.setEnabled(False)
        self.addTempButton.setEnabled(False)
        # actualizar lista de temperaturas en la gui
        self.actualizar_temps()
        # colocar la interfaz en la pestaña Parámetros.
        self.mainTabWidget.setCurrentIndex(0)
        # Colocar una ventana de instrucciones al inicio del sistema.
        if self.opciones.value('Helps') != '0':
            ventana = ayudasWind(
                "Ayuda - Bipylvo / Como Usarlo.",
                [
                    'Recursos/docs/acercaDeBipylvo.md',
                    'Recursos/docs/comoUsar.md',
                ],
                parent=self,
            )
            ventana.show()
            if self.opciones.value('Helps') is None:
                self.opciones.setValue('Helps', '0')
        # Fin del constructor
        return

    # ------------------------------------------------------------------
    def centrar(self):
        """Función "MainWind.centrar".

        Re-dimensionar (según las opciones guardadas en la ultima ejecución
        de la ventana) y ubicar la aplicación en el medio de la pantalla.

        Parámetros: (self)
        """
        self.resize(self.opciones.value('size', QSize(480, 550)))
        pantalla = QDesktopWidget().screenGeometry()
        tam = self.geometry()
        self.move(
                    (pantalla.width()-tam.width())/2,
                    (pantalla.height()-tam.height())/2,
        )
        return
    # ------------------------------------------------------------------

    def ayudasCheckMenuButton(self):
        """Función "MainWind.ayudasCheckMenuButton".

        Permite agregar o cancelar la función de solicitar ayudas al inicio
        de la interfaz de la simulación.

        Parámetros: (self)
        """
        if self.initAyudaMenuApp.isChecked():
            self.opciones.setValue('Helps', '1')
        else:
            self.opciones.setValue('Helps', '0')
        return
    # ------------------------------------------------------------------

    def cambios_especie(self, ind):
        """Función "MainWind.cambios_especie".

        Maneja el evento de cambio de especie en el QCombobox de la ventana.
        Recibe un indice referente a una posición en el selector.

        Parámetros: (self, ind)
        """
        self.simularButton.setEnabled(False)
        self.argsim['grup'] = None
        if ind == 0:
            # Si no se elige ninguna especie se cancela el contenido de los
            # selectores en el área de parámetros.
            self.argsim['esp'] = None
            self.grupoetarioComboBox.clear()
            self.grupoetarioComboBox.setEnabled(False)
            # Evitar que puedan agregar temperaturas
            self.addTempButton.setEnabled(False)
            self.actualizar_temps()
        else:
            # Colocar la clase de la especie segun el contenido del selector
            self.argsim['esp'] = modulos[self.especieComboBox.currentText()]
            grupos = sorted(list(self.argsim['esp'].etapas_nomb.keys()))
            self.grupoetarioComboBox.clear()
            self.grupoetarioComboBox.setEnabled(True)
            self.grupoetarioComboBox.addItems(grupos)

            # Se ajustan los limites de alimento
            # Mínimo
            lim = self.argsim['esp'].limites['alimMin']
            self.alimentoDoubleSpinBox.setMinimum(lim)
            # Máximo
            lim = self.argsim['esp'].limites['alimMax']
            self.alimentoDoubleSpinBox.setMaximum(lim)
            # Permitir agregar temperaturas
            self.addTempButton.setEnabled(True)
            self.actualizar_temps()

        return
    # ------------------------------------------------------------------

    def cambios_grupoetario(self, ind):
        """Función "MainWind.cambios_grupoetario".

        Maneja los cambios en el QCombobox para los grupos etarios de la
        especie de molusco seleccionado por el usuario.
        La función recibe un argumento indice que determina la posición en
        el selector.

        Parámetros: (self, ind)
        """
        if self.argsim['esp']:
            esp = self.argsim['esp']
            temp1 = self.grupoetarioComboBox.currentText()
            temp2 = esp.etapas_nomb[temp1]
            if temp2 in esp.etapas.keys():
                self.argsim['grup'] = temp2
            else:
                texto = "Error: %s no es un grupo etario valido." % temp1
                sys.stdout.write(texto)
                self.error_msj(texto, "Grupo Etario")
                self.argsim['grup'] = None

        self.actualizar_temps()
        return

    # ------------------------------------------------------------------
    def actualizar_temps(self):
        """Función "actualizar_temps" para mostrar las temperaturas al usuario.

        Utiliza los datos cargados en la lista de temperaturas y los coloca en
        la lista de temperaturas de la interfaz. (La función se ejecuta en los
        diálogos que modifican las temperaturas)
        """
        # cargar el modelo de la lista de datos.
        self.temperaturaListWidget.clear()
        if self.argsim['temps']:
            ind = 1
            for temperatura in self.argsim['temps']:
                self.temperaturaListWidget.addItem(
                    QListWidgetItem("%d) \t %s °C" % (ind, str(temperatura))),
                )
                ind += 1
        else:
            texto = "No se tienen temperaturas registradas."
            self.temperaturaListWidget.addItem(
                QListWidgetItem(texto),
            )

        if self.argsim['temps'] and self.argsim['esp'] and self.argsim['grup']:
            self.rmTempButton.setEnabled(True)
            self.simularButton.setEnabled(True)
            self.simularButton.setStyleSheet(
                """
                QPushButton
                {background-color: #689f38; color: #FEFEFE;}

                QPushButton::hover
                {background-color: #8bc34a; color: #000;}

                QPushButton::pressed
                {background-color: #33691e; color: #FEFEFE;}
                """,
            )
        else:
            self.rmTempButton.setEnabled(False)
            self.simularButton.setEnabled(False)
            self.simularButton.setStyleSheet('')

        return

    # ------------------------------------------------------------------
    def agregar_temp(self):
        """Función "agregar_temp" maneja el evento de agregar temperatura.

        Esta función llama al dialogo para agregar temperatura que permite
        cargar nuevas temperaturas y registrarlas en la lista de temperaturas
        a usar en la simulación que desea realizar.
        """
        alerta = AgregarDialog(self.argsim["esp"], parent=self)
        if not alerta.error:
            alerta.exec_()
        # actualizar lista de temperaturas en la gui
        self.actualizar_temps()
        return

    # ------------------------------------------------------------------
    def remover_temp(self):
        """Función "remover_temp" remueve el objeto seleccionado en la lista.

        Es la función designada para remover una temperatura de la lista de
        temperaturas en los parámetros de la simulación.
        """
        if not self.argsim['temps']:
            texto = 'Error. no hay temperaturas registradas actualmente' \
                ' en los parámetros.'
            self.error_msj(texto, 'Temperaturas')
        # obtener objetivo a remover
        indice = self.temperaturaListWidget.currentRow()
        if indice is not None:  # si hay algo seleccionado
            # remover objetivo
            self.argsim['temps'].pop(indice)
        else:  # si no.
            texto = "Error. Debe seleccionar en la lista la temperatura " \
                "que desea remover."
            self.error_msj(texto, "Temperaturas")
        # actualizar lista de temperaturas en la gui
        self.actualizar_temps()

        return

    # ------------------------------------------------------------------
    def simular(self):
        """Función "MainWind.simular".

        Maneja el evento de clic en el botón simular y ejecuta la simulación
        según los parámetros determinados por el usuario.
        Nota: toma los parámetros a través de "self".

        Parámetros: (self)
        """
        # Extraer los parámetros a simular
        self.argsim['alim'] = self.alimentoDoubleSpinBox.value()
        self.argsim['tiemp'] = self.tiempoSpinBox.value()
        self.argsim['cant'] = self.cantidadSpinBox.value()
        # Comparar si el alimento dado en los parámetros
        # esta entre los limites del animal.
        cond1 = self.argsim['alim'] < self.argsim['esp'].limites['alimMin']
        cond2 = self.argsim['alim'] > self.argsim['esp'].limites['alimMax']
        # Tratamiento de los datos en para el limite de alimento
        if cond1 or cond2:
            temp1 = self.argsim['esp'].limites['alimMin']
            temp2 = self.argsim['esp'].limites['alimMax']
            texto = "Esta especie tiene un limite de alimento de " \
                "Min: %f Max: %f, ajuste su valor actual [%f]"
            self.error_msj(
                    texto % (temp1, temp2, self.argsim['alim']),
                    "Alimento",
            )
            return
            # de haber un error en el alimento la simulación se cancela

        # Conversión y comprobación de las
        # temperaturas ingresadas por el usuario.
        error = list()
        for i in range(len(self.argsim['temps'])):
            try:
                # Se verifica el uso de comas o puntos para formar los números
                temp = self.argsim['temps'][i]
                # if (',' in temp) and not ('.' in temp):
                #    temp = temp.replace(',', '.')
                # elif(',' in temp) and ('.' in temp):
                #    temp = temp.replace(',', '')
                # Se transforma el dato en número flotante (posible excepción)
                self.argsim['temps'][i] = float(temp)

                lmax = self.argsim['esp'].limites['tempMax']
                lmin = self.argsim['esp'].limites['tempMin']
                cond1 = self.argsim['temps'][i] > lmax
                cond2 = self.argsim['temps'][i] < lmin
                if cond1 or cond2:
                    texto = 'Error Temp:%f: La Temperatura del ' \
                        'Ambiente debe estar entre %f°C y %f°C'
                    raise EnvironmentError(
                        texto %
                        (self.argsim['temps'][i],
                         self.argsim['esp'].limites['tempMin'],
                         self.argsim['esp'].limites['tempMax'],
                         ),
                    )
            except ValueError:
                error.append(
                    "Error Temp:%s: No puede ser transformado a un numero." %
                    str(self.argsim['temps'][i]),
                )
            except EnvironmentError as err:
                error.append(str(err))
            except Exception as err:
                error.append(
                    "Error Inesperado en Temperatura: %s." %
                    str(self.argsim['temps'][i]),
                )
        if error:
            sys.stdout.write(str(error))
            if len(error) > 1:
                error = str(error).replace(",", "\n\n - ")
                error = "Listado de Errores:\n" + error
            self.error_msj(
                str(error)[:-1].replace('[', '-'),
                "Temperaturas",
            )
            return

        # Continua la comprobación.
        try:
            self.argsim['tempsEnDias'] = \
                temperaturas_para_el_tiempo_dic(self.argsim)
            if isinstance(self.argsim['tempsEnDias'], ndarray):
                self.argsim['tempsEnDias'] = \
                    self.argsim['tempsEnDias'].tolist()
        except Exception as err:
            sys.stdout.write("%s \n" % str(err))
            self.error_msj(str(err), "Temperaturas")
            return

        # Creación del ambiente de simulación y su ejecución.
        self.bivalvos = list()
        env = simpy.Environment()
        # Nota Solo se deben usar ID de formato númerico, solo números.
        for ID in range(0, int(self.argsim['cant'])):
            self.bivalvos.append(
                self.argsim['esp'](
                    ID+1, self.argsim['tempsEnDias'],
                    self.argsim['alim'],
                    self.argsim['grup'],
                ),
            )
            env.process(self.bivalvos[ID].simular_acuario(env))

        # Registrar y ejecutar dialogo con barra progreso
        alerta = QProgressDialog(
            "Ejecutando la Simulación",
            "Cancelar", 0, 100, self)
        alerta.setWindowTitle("Bipylvo - Simulación en ejecución")
        alerta.setWindowModality(Qt.WindowModal)
        alerta.setMinimumWidth(300)
        alerta.setAutoClose(True)
        alerta.show()

        for tiemp in range(1, int(self.argsim['tiemp'])+1):
            # ejecutar los procesos paso a paso
            env.run(until=tiemp)
            # actualizar la barra
            alerta.setValue(int((env.now*100)/int(self.argsim['tiemp'])))
            # si el usuario cancela
            if alerta.wasCanceled():
                # Salir de la simulación sin presentar datos
                self.error_msj(
                    "Simulación Cancelada",
                    descrip="La ejecución de la simulación fue cancelada.",
                )
                return
        # Fin del "for"

        # Solicitar los datos para la tabla.
        resultados_tabulados = list()
        for bivalvo in self.bivalvos:
            resultados_tabulados.append(bivalvo.tabulado())
        # fin del for.
        # Actualizar Banderas
        self.tiene_actividad = True
        self.detallesid_flag = False
        self.detallesdia_flag = False
        self.graficasbox_flag = True
        # Imprimir los datos en la interfaz.
        self.listar_parametros()
        self.listar_estadisticas()
        self.poblar_simulTabla(resultados_tabulados)
        self.poblar_detalles_eventos(0)
        # Permitir usar los botones en el simulador.
        self.graficarComboBox.setEnabled(True)
        self.graficarButton.setEnabled(True)
        self.detalles_idSpinBox.setEnabled(True)
        self.detalles_diaSpinBox.setEnabled(True)

        # Cambiar la pestaña en uso en el simulador.
        # Parámetros = 0, Simulador = 1, Detalles = 2
        self.mainTabWidget.setCurrentIndex(1)
        return

    # ------------------------------------------------------------------
    def poblar_simulTabla(self, resultados_tabulados):
        """Función "MainWind.poblar_simulTabla".

        Esta función recibe los datos a tabular y los utiliza para poblar la
        Tabla de la simulación que fue ejecutada.

        Parámetros: (self, resultados_tabulados)
        """
        if resultados_tabulados:
            # Limpiar los datos que existan en la tabla. (Reemplazar el Modelo)
            modeloTabla = QStandardItemModel()
            # Se colocan las cabeceras correspondientes.
            modeloTabla.setHorizontalHeaderLabels(
                self.argsim['esp'].cabeceras_para_tablas,
            )
            modeloTabla.setRowCount(len(resultados_tabulados))

            # Colocar datos en la tabla.
            x = 0
            for linea in resultados_tabulados:
                y = 0
                for dat in linea:
                    celda = QStandardItem(str(dat))
                    celda.setTextAlignment(Qt.AlignCenter)
                    celda.setEditable(False)
                    modeloTabla.setItem(x, y, celda)
                    y += 1
                x += 1
            # Grabar modelo a la tabla y terminar.
            self.simulTableView.setModel(modeloTabla)
            self.simulTableView.resizeColumnsToContents()
            self.modificar_splitter(self.size().width(), self.size().height())
            self.simulTableView.move(0, 0)
        else:
            texto = "Error inesperado. " \
                "Nada que colocar en la tabla de resultados."
            self.error_msj(texto, descrip="No hay resultados a mostrar")
            sys.stdout.write(texto)
            return

    # ------------------------------------------------------------------
    def evento_click_tabla(self, indice):
        """Función "MainWind.evento_click_tabla".

        Evento de "clic" o selección de un miembro de la tabla de resultados.
        Recibe un parámetro "indice" obligatorio por el evento, indica la
        posición en la tabla.

        Parámetros: (self, indice)
        """
        if self.tiene_actividad:
            self.graficasbox_flag = True
            modelo = indice.model()
            try:
                bivalvo_ID = int(modelo.item(indice.row()).text())
                self.poblar_detalles_eventos(bivalvo_ID)
            except IndexError as err:
                texto = "Error: Datos inválidos, " \
                    "el ID no coincide con un bivalvo simulado."
                sys.stdout.write(texto)
                sys.stdout.write("%s \n" % str(err))
                self.error_msj(texto)
            except Exception as err:
                sys.stdout.write("%s \n" % str(err))
                texto = "Error Inesperado al asignar los " \
                    "detalles de un Bivalvo (ID:%s)" % bivalvo_ID
                self.error_msj(texto, descrip=str(err))
            else:
                self.mainTabWidget.setCurrentIndex(2)
        return

    # ------------------------------------------------------------------
    def evento_detalles_udp_clicked(self):
        """Función "MainWind.evento_detalles_udp_clicked".

        Evento de "clic" del botón para actualizar el bivalvo detallado.

        Parámetro: (self)
        """
        if not self.tiene_actividad:
            texto = "Error: No hay una simulación activa para mostrar datos"
            sys.stdout.write(texto)
            self.error_msj(texto)
            return

        ID = self.detalles_idSpinBox.value()
        dia = self.detalles_diaSpinBox.value()
        self.graficasbox_flag = True
        try:
            self.poblar_detalles_eventos(ID, dia)
        except IndexError as err:
            texto = "Error: Datos inválidos, " \
                "el ID no coincide con un bivalvo simulado."
            sys.stdout.write(texto)
            sys.stdout.write("%s \n" % str(err))
            self.error_msj(texto, campo='Bivalvo ID')
        except Exception as err:
            sys.stdout.write("%s \n" % str(err))
            texto = "Error Inesperado al asignar los " \
                "detalles de un Bivalvo (ID:%s, Día:%s)" % (ID, dia)
            self.error_msj(texto, descrip=str(err))
        return

    # ------------------------------------------------------------------
    def poblar_detalles_eventos(self, ID, dia='ult'):
        """Función "MainWind.poblar_detalles_eventos".

        Actualizar listado de detalles de un bivalvo, necesita el ID del
        bivalvo y el día de la simulación. Si el día es 0 entonces muestra los
        datos iniciales. Si el día no es un numero o no es 0 entonces usa el
        ultimo día.

        Parámetros: (self, ID, dia='ult')
        """
        if str(ID).isdigit():
            try:
                ID = int(ID) - 1
            except (ValueError, TypeError) as err:
                sys.stdout.write("%s \n" % str(err))
                texto = "El ID del Bivalvo %s no es un numero." % str(ID)
                sys.stdout.write(texto)
                self.error_msj(
                    texto,
                    campo="Bivalvo ID (Pestaña Detalles)",
                    descrip=str(err),
                )
            else:
                if ID < 0:
                    ID = 0

        if not self.detallesid_flag or not self.detallesdia_flag:
            # Evitar que el programa disparé eventos
            # que son solo para el usuario.
            self.detallesid_flag = True
            self.detallesdia_flag = True
            # (Re)definir los limites de los IDs disponibles y de los días.
            self.detalles_idSpinBox.setMaximum(len(self.bivalvos))
            self.detalles_diaSpinBox.setMaximum(self.bivalvos[ID].dias)
            # obtener los datos requeridos según el ID.
            if dia == 0:
                datos = self.bivalvos[ID].ini
            elif str(dia).isdigit() and dia != self.bivalvos[ID].dias:
                try:
                    datos = self.bivalvos[ID].hist[int(dia) - 1]
                except IndexError as err:
                    datos = self.bivalvos[ID]
                    sys.stdout.write("%s \n" % str(err))
                    texto = "El Bivalvo no fue simulado para el día: %s." \
                        " Cargando el dato del ultimo día simulado." % str(dia)
                    sys.stdout.write(texto)
                    # self.error_msj(
                    #    texto,
                    #    campo="Día (Pestaña Detalles)",
                    #    descrip=str(err),
                    # )
                except Exception as err:
                    datos = self.bivalvos[ID]
                    sys.stdout.write("%s \n" % str(err))
                    texto = "Error Inesperado al asignar los detalles " \
                        "de un Bivalvo (ID:%s, Día:%s). Cargando el dato" \
                        " del ultimo día simulado." % (str(ID), str(dia))
                    sys.stdout.write(texto)
                    self.error_msj(texto, descrip=str(err))
            else:
                datos = self.bivalvos[ID]

            # Actualizar los datos mostrados en los SpinBox para ID.
            self.detalles_idSpinBox.setValue(self.bivalvos[ID].ani["ID"])
            self.detalles_diaSpinBox.setValue(datos.dias)
            # Agregar gráficas disponibles.
            self.graficasbox_flag = True
            self.graficarComboBox.clear()
            self.graficarComboBox.addItems(sorted(datos.graficas.keys()))
            # Agregar filas
            lista = datos.tabulado()
            # Limpiar los datos que existan en la tabla. (Reemplazar el Modelo)
            modeloTabla = QStandardItemModel()
            # Se colocan las cabeceras correspondientes.
            modeloTabla.setRowCount(len(lista))
            modeloTabla.setVerticalHeaderLabels(datos.cabeceras_para_tablas)
            # Colocar datos en la tabla.
            x = 0
            for dat in lista:
                celda = QStandardItem(str(dat))
                celda.setTextAlignment(Qt.AlignCenter)
                celda.setEditable(False)
                modeloTabla.setItem(x, 0, celda)
                x += 1
            # fin del for

            # Grabar modelo a la tabla.
            self.detallesTablaView.setModel(modeloTabla)
            self.detallesTablaView.horizontalHeader().setSectionResizeMode(
                QHeaderView.Stretch)
            self.detallesTablaView.move(0, 0)
            # Datos para la lista de eventos:
            self.eventosListWidget.clear()
            if self.bivalvos[ID].even:
                for evento in self.bivalvos[ID].even:
                    if ':' in str(evento):
                        dato = str(evento).split(':')
                        self.eventosListWidget.addItem(
                            QListWidgetItem(
                                "%s (día: %s)" % (dato[0], dato[1])),
                        )
                    else:
                        self.eventosListWidget.addItem(
                            QListWidgetItem(str(evento)))
            else:
                texto = "No se tienen eventos registrados " \
                    "durante esta simulación"
                self.eventosListWidget.addItem(
                    QListWidgetItem(texto),
                )
        # Fin del If (evitar uso)
        # Colocar las banderas para usar la interfaz
        self.detallesid_flag = False
        self.detallesdia_flag = False
        self.graficasbox_flag = False
        return

    # ------------------------------------------------------------------
    def ayudaUso(self):
        """Función "MainWind.error_msj".

        Llama a la ventana de ayuda con la información del uso del simulador.
        Parámetros: (self)
        """
        ventana = ayudasWind(
            "Ayuda - Uso del simulador",
            'Recursos/docs/comoUsar.md',
            parent=self,
        )
        ventana.show()
        return

    # ------------------------------------------------------------------
    def acercaDeBipylvo(self):
        """Función "MainWind.error_msj".

        Llama a la ventana de ayuda con la información del simulador.
        Parámetros: (self)
        """
        ventana = ayudasWind(
            "Acerca del simulador",
            'Recursos/docs/acercaDeBipylvo.md',
            parent=self,
        )
        ventana.show()
        return

    # ------------------------------------------------------------------
    def error_msj(self, msj=None, campo=None, descrip=None):
        """Función "MainWind.error_msj".

        Formato predefinido para informar de errores al usuario. Necesita
        recibir un mensaje en formato de texto, pero también recibe un "campo"
        opcional en caso de errores en un campo de los parámetros en la
        interfaz gráfica, y por ultimo un campo opcional para una descripción.

        Parámetros: (self, msj=None, campo=None, descrip=None)
        """
        if msj:
            # Alertar sobre errores
            alerta = QMessageBox(self)
            alerta.setIcon(QMessageBox.Warning)
            alerta.setWindowTitle("Bipylvo - Alerta de Error")
            alerta.setText(str(msj))
            alerta.setWindowModality(Qt.WindowModal)
            alerta.setMinimumWidth(400)
            if campo:
                texto = "Debe corregir el error en el campo: \n" \
                    " %s  para poder continuar."
                alerta.setInformativeText(texto % campo)
            elif descrip:
                alerta.setInformativeText(str(descrip))
            else:
                alerta.setInformativeText(
                    "Corrija el error en el rango de " +
                    "entradas para poder continuar.")
            alerta.setStandardButtons(QMessageBox.Close)
            alerta.button(QMessageBox.Close).setText("Cerrar Alerta")
            alerta.setDefaultButton(QMessageBox.Close)
            alerta.exec_()
        return

    # ------------------------------------------------------------------
    def listar_parametros(self):
        """Función "MainWind.listar_parametros".

        Lista las variables iniciales recolectadas de la simulación realizada.
        """
        # Limpiar la lista.
        self.simulparamList.clear()
        # Agregar parámetros
        self.simulparamList.addItem(
            QListWidgetItem(
                "Tiempo a simular:\t%s" % self.argsim['tiemp']),
        )
        self.simulparamList.addItem(
            QListWidgetItem(
                "Cantidad de bivalvos:\t%s" % self.argsim['cant']),
        )
        self.simulparamList.addItem(
            QListWidgetItem(
                "Cantidad de alimento:\t%s" % self.argsim['alim']),
        )
        # Parámetro del grupo etario:
        try:
            temp = self.argsim['esp'].etapas_nomb.items()
            etapa = next(
                clave for clave, valor in temp
                if valor == self.argsim['grup']
            )
        except StopIteration:
            texto = "Error. Grupo Etario [ %s ] desconocido " \
                "en los parámetros iniciales." % str(self.argsim['grup'])
            sys.stdout.write(texto)
            self.error_msj(texto)
        except Exception as err:
            texto = "Error Desconocido en los parámetros iniciales."
            sys.stdout.write(texto)
            sys.stdout.write("%s \n" % str(err))
            self.error_msj(texto, descrip=str(err))
        else:
            if ") " in etapa:
                etapa = str(etapa).split(") ")[-1]

            self.simulparamList.addItem(
                QListWidgetItem("Grupo etario inicial:\t%s" % etapa),
            )
        return

    # ------------------------------------------------------------------
    def listar_estadisticas(self):
        """Función "MainWind.listar_estadisticas".

        Lista las estadísticas sobre las variables recolectadas durante
        la simulación.
        """
        # Limpiar la lista.
        self.simulstatsList.clear()
        # Ejecutar la función de calculo de estadísticas.
        filas = promediosYvarianzas(self.bivalvos)
        # Agregar parámetros
        for fila in filas:
            self.simulstatsList.addItem(
                QListWidgetItem(str(fila)),
            )
        return

    # ------------------------------------------------------------------
    def evento_grafica_de_datos(self):
        """Función "evento_grafica_de_datos" despliega la gráfica solicitada.

        Este evento utiliza las funciones incluidas en el modelo de simulación
        del bivalvo seleccionado para crear gráficas de los parámetros del
        animal, permitiendo al usuario estudiar el comportamiento de la
        simulación. Requiere que se haya ejecutado una simulación y se tenga
        seleccionado un bivalvo en especifico.
        """
        # Evitar que el programa se ejecute si el usuario no pide la gráfica.
        if self.graficasbox_flag:
            return
        # Fin if

        # Cargar los datos desde los formularios de entrada.
        ID = self.detalles_idSpinBox.value() - 1  # Valor en pestaña detalles.
        tipo = self.graficarComboBox.currentText()  # gráficas del animal.
        # Intentar encontrar el animal simulado.
        try:
            individuo = self.bivalvos[ID]
        except IndexError as err:
            # error si el ID no esta en la lista
            texto = "El Bivalvo con el ID: %d no fue simulado." % ID
            sys.stdout.write(texto)
            sys.stdout.write("%s \n" % str(err))
            self.error_msj(
                texto,
                campo="Bivalvo ID (Pestaña Detalles)",
                descrip=str(err),
            )
        except Exception as err:
            # Error desconocido
            texto = "Error desconocido al asignar un individuo" \
                " para gráficar"
            sys.stdout.write(texto)
            sys.stdout.write("%s \n" % str(err))
            self.error_msj(
                texto,
                descrip=str(err),
            )
        else:
            # Si lo encuentra entonces:
            # verificar si existe la gráfica para ese individuo
            if tipo in individuo.graficas.keys():
                (  # Asignar variables a gráficar
                    titulo,
                    datos,
                    leyenda_x,
                    leyenda_y,
                ) = individuo.graficas[tipo]()
                # Ejecutar gráfica y desplegar el dialogó.
                grafica = GraficosDialog(
                    titulo, datos, leyenda_x, leyenda_y, parent=self)
                grafica.show()
            else:
                # De no existir el tipo de gráfica, pintar error
                texto = "Error, gráfica no definida para" \
                    " esta la especie: %s" % individuo.nombre
                sys.stdout.write(texto)
                self.error_msj(
                    texto,
                    campo="Gráficas Disponibles (Pestaña detalles)",
                )
        # Fin try

        return

    # ------------------------------------------------------------------
    def modificar_splitter(self, ancho, alto):
        """Función "MainWind.modificar_splitter".

        Modifica la orientación de los Qsplitter en la pestaña de simulación
        según el ancho de la ventana.

        Parámetros: (self, ancho, alto)
        """
        if alto <= ancho-150 and ancho >= 400:
            self.simulSplitter.setOrientation(1)
            self.simulSplitter.setSizes([ancho*0.25, ancho*0.75])
            self.paramsSplitter.setOrientation(0)
            self.paramsSplitter.setSizes([alto*0.5, alto*0.5])
        else:
            self.simulSplitter.setOrientation(0)
            self.simulSplitter.setSizes([alto*0.25, alto*0.75])
            self.paramsSplitter.setOrientation(1)
            self.paramsSplitter.setSizes([ancho*0.5, ancho*0.5])

        return

    # ------------------------------------------------------------------
    # de la ventana.
    def resizeEvent(self, evento):
        """Función "MainWind.resizeEvent".

        Evento de para reorganizar la distribución de la interfaz según el
        tamaño de la ventana. Recibe el parámetro "evento" proveniente de la
        ventana al ser modificada, el mismo es del tipo QEvent.

        Parámetros: (self, evento)
        """
        self.modificar_splitter(
            ancho=evento.size().width(),
            alto=evento.size().height(),
        )
        evento.accept()
        return

    # ------------------------------------------------------------------
    def closeEvent(self, evento):
        """Función "MainWind.closeEvent".

        Modifica el evento de cierre para guardar las opciones
        y datos de la aplicación. Requiere un "evento" en formato QEvent.

        Parámetros: (self, evento)
        """
        if self.tiene_actividad:
            # Crear alerta de cierre del programa
            alerta = QMessageBox(self)
            alerta.setIcon(QMessageBox.Critical)
            alerta.setWindowTitle("Bipylvo - Alerta de Salida")
            alerta.setText(
                "¿Esta seguro de que quiere salir de la aplicación?")
            alerta.setInformativeText(
              "Tiene datos sin guardar, y se perderán al cerrar el programa.",
            )
            alerta.setStandardButtons(QMessageBox.Close | QMessageBox.Cancel)
            alerta.button(QMessageBox.Close).setText("Cerrar y Descartar")
            alerta.button(QMessageBox.Cancel).setText("Cancelar")
            alerta.setDefaultButton(QMessageBox.Cancel)
            alerta.setWindowModality(Qt.WindowModal)
            # Ejecutar la alerta
            respuesta = alerta.exec_()
            if respuesta == QMessageBox.Close:
                self.opciones.setValue('size', self.size())
                evento.accept()
            else:
                evento.ignore()
        else:
            self.opciones.setValue('size', self.size())
            evento.accept()
        return

    # ------------------------------------------------------------------

    # ------------------------------------------------------------------
# ----------------------------------------------------------------------
