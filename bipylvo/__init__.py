"""
Archivo "__init__".

Le permite a python reconocer la estructura del proyecto en forma de módulos
del propio lenguaje, y contiene la versión actual de la aplicación Bipylvo.

La versión del proyecto esta guardada en la variable "__version__", como una
cadena de caracteres y debe mantener la siguiente sintaxis:

    int.int.int-YYYYMMDD

Donde "int" es un entero mayor que cero (0), "YYYY" son los cuatro (4) dígitos
referentes al año actual, MM son los dos (2) dígitos del mes actual, y DD son
los dos (2) dígitos del día actual.
"""
__version__ = "0.1.1-20171111"
