#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Archivo "main.pyw" del Simulador Bipylvo.

Bipylvo es un proyecto experimental con el fin de crear un software para la
Simulación del gasto energético en distintas etapas del ciclo de vida de la
vieira "Euvola ziczac", aunque puede ser utilizado para crear simuladores para
otras especies de bivalvos. La simulaciones que este programa busca implementar
son de tipo Montecarlo, y de variables discretas.

Este archivo contiene la declaración de la instancia de QT de la aplicación
bipylvo y su ejecución.

Las dependencias de este programa son: PyQt5, Simpy, Matplotlib, os, sys,
signal, copy, math.
"""
# --------------------------------Libs----------------------------------
from __future__ import unicode_literals

import sys

from os import path

from platform import system as currentSys

from signal import SIGINT, SIG_DFL, signal

from GUI.mainUI import MainWind

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication, QMessageBox
# ----------------------------------------------------------------------


# --------------------------------Main----------------------------------
def main(args):
    """Declaración del "Main" para el simulador Bipylvo.

    Este que dispara la ejecución de la aplicación hecha en Pyqt5
    """
    # Signal recibe las indicaciones de cerrar el
    # script de forma abrupta si es necesario.
    signal(SIGINT, SIG_DFL)

    app = QApplication(args)
    app.setDesktopSettingsAware(True)
    # Requiere colocar a python2 en utf8, python3 no lo requiere.
    # El programa requiere Python3.
    if sys.version_info < (3, 0):
        texto = "Este programa fue diseñado para Python3 como mínimo."
        sys.stderr.write(texto)
        alerta = QMessageBox(None)
        alerta.setIcon(QMessageBox.Critical)
        alerta.setWindowTitle("Bipylvo - Alerta de Error.")
        alerta.setText(texto)
        alerta.setWindowModality(Qt.WindowModal)
        alerta.setMinimumWidth(320)
        alerta.setStandardButtons(QMessageBox.Close)
        alerta.button(QMessageBox.Close).setText("Cerrar Alerta")
        alerta.setDefaultButton(QMessageBox.Close)
        alerta.exec_()
        return 0

    # si es windows hay que colocar un estilo
    if currentSys() == 'Windows':
        app.setStyle('Fusion')

    if getattr(sys, 'frozen', False):
        # si Bipylvo esta corriendo en un único archivo Ejecutable
        direc = sys._MEIPASS
    else:
        direc = path.dirname(path.realpath(__file__))
    ventana = MainWind(parent=None, direc=direc)
    ventana.show()
    app.exec_()
    return 0
# ----------------------------------------------------------------------


# ------------------------------Llamada---------------------------------
if __name__ == '__main__':
    sys.exit(main(sys.argv))
# ----------------------------------------------------------------------
