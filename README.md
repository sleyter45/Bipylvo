# PROYECTO BIPYLVO
[![Bipylvo](https://gitlab.com/sleyter45/Bipylvo/raw/master/bipylvo/Recursos/ico/bipylvo-logox128.png)](https://gitlab.com/sleyter45/Bipylvo/) [![DLI-UDONS](https://gitlab.com/sleyter45/Bipylvo/raw/master/bipylvo/Recursos/ico/LogoInfo.png)](http://herbario.sucre.udo.edu.ve/index.php) [![UDONS](https://gitlab.com/sleyter45/Bipylvo/raw/master/bipylvo/Recursos/ico/logo-udo.png)](http://www.sucre.udo.edu.ve/)

[Bipylvo](https://gitlab.com/sleyter45/Bipylvo/) es un proyecto de desarrollo de un modelo de simulación capaz de emular el comportamiento fisiológico de la vieira *Euvola ziczac*, basado en modelos matemáticos como la Formula del Balance Energético (SFG) y la ecuación de crecimiento de *von Bertalanffy*, además del conocimiento  sobre el desarrollo y comportamiento del bivalvo *Euvola ziczac* obtenido en el Centro de Investigaciones en Biología y Acuicultura de Moluscos (CIBAM) de la [Universidad de Oriente Núcleo de Sucre (UDONS)](http://www.sucre.udo.edu.ve/). El modelo es aplicado para el desarrollo y ejecución de un Software de simulación discreto del tipo **Montecarlo** y aplicando la metodología de desarrollo Proceso de Solución de Problemas en CA/IO. Para esto se emplearon el lenguaje de programación general *Python*, además de la librería *Simpy* para el manejo de los ciclos de simulación, con una interfaz gráfica basada en la librería *Qt 5* y sus respectiva  implementación para el lenguaje llamada *PyQt5*; además de la capacidad para generar gráficas estadísticas usando la librería *Matplotlib*. Este software está desarrollado como un Software Libre con propósitos académicos, entre ellos, la de servir de herramienta de apoyo para la investigación y enseñanza en el Centro de Investigación de la Biología y Acuicultura de moluscos (CIBAM).

## Objetivo del Simulador:
El propósito principal del simulador es permitir al usuario **realizar la ejecución de la formula del Balance Energético o S.F.G.(*Scope for Growth*)** en las especies de bivalvos que se tengan a disposición, sin embargo el sistema nació para de simular el Balance Energético de la vieira *Euvola ziczac*

### La Fórmula del Balance Energético o *Scope for Growth (SFG)*:
Es un modelo matemático que permite observar el equilibrio alcanzado por el organismo entre la energía obtenida a través del alimento, y la perdida debido al gasto metabólico, evidenciado mediante la respiración y la excreción. Una vez satisfecho esto, la energía remanente puede ser usada para el desarrollo, crecimiento, reproducción, entre otros, de cada individuo. El trabajo de Widdows & Johnson (1988) muestra el modelo como:
```P = A - (R + U)```
  > Donde “`P`” representa la energía sobrante y también es representada como **S.F.G.**, “`A`” representa la energía absorbida del alimento ingerido, “`R`” gasto energético en la respiración, y “`U`” representa la energía perdida como excreta. Los resultados son mostrados en unidades de energía (Joules) y el modelo puede ser aplicado para cualquier bivalvo.

Mediante el empleo del **S.F.G.** se puede apreciar la cantidad energía obtenida por el bivalvo bajo estudio, y que luego sería distribuida por el organismo. La asignación de energía entre el mantenimiento, crecimiento y reproducción es una parte central de la estrategia de la vida (Cody, 1966; Levins, 1968; Sibly & Calow, 1986; Williams, 1966a, b). Por supuesto, la asignación de energía es un proceso dinámico que depende de una multitud de factores ecológicos y fisiológicos (Sibly & Calow, 1986; Stearns, 1992).

## Acerca del Autor:
Creado por [*Luis J. Freites E.*](mailto://sleyter45@gmail.com), [Bipylvo](https://gitlab.com/sleyter45/Bipylvo/) es un software de simulación para entorno gráfico, de propósito de investigación y educativo, desarrollado como parte de la tesis de grado en la [Universidad de Oriente Núcleo de Sucre (UDONS)](http://www.sucre.udo.edu.ve/), para la [Departamento de Licenciatura en Informática](http://herbario.sucre.udo.edu.ve/index.php) y el Centro de Investigaciones en Biología y Acuicultura de Moluscos (CIBAM).

### Licencia:
 MIT [ver: LICENSE.txt](https://gitlab.com/sleyter45/Bipylvo/raw/master/LICENSE.txt)

  > **Software Libre, pero el copyright queda para la UDONS debido lo estipulado en el reglamento de Trabajo de Pregrado**

## Instalación:
Actualmente existen defectos de instalación y el script creado "setup.py" no puede instalar el sistema por que no integra todas las dependencias del mismo (Pyqt5 y SIP específicamente), por lo que se distribuye el sistema de manera manual con el Este GIT o con la ayuda de [PyInstaller](http://www.pyinstaller.org/).

### Instalación manual:

1.  Instalar [GIT](https://git-scm.com) para descargar el proyecto dependiendo del sistema operativo usado: [Pagina oficial de GIT](https://git-scm.com/downloads)
1.  Instalar [Python3](https://www.python.org) para su sistema operativo:
    -   GNU/Linux Debian, o Ubuntu: (ejecutar comandos como `Root`)
        ```apt-get install python3 python3-pip```
    -   Windows necesita descargarlo manualmente de:
        [Python 3.6 para Windows 32Bit](https://www.python.org/ftp/python/3.6.3/python-3.6.3.exe) o [Python 3.6 para Windows 64Bit](https://www.python.org/ftp/python/3.6.3/python-3.6.3-amd64.exe)

    >   Nota Importante: en ambos sistemas Windows, en el instalador debe marcar la opción "Add Python to PATH".

1.  Descargar el proyecto Bipylvo con GIT (ejemplo usando los comandos):
    ```git clone https://gitlab.com/sleyter45/Bipylvo.git```

1.  Actualizar el programa (Opcional): ```git pull```

1.  Instalar dependencias de Python3:
    ```pip3 install PyQt5 SIP argparse numpy simpy matplotlib pyinstaller Cython markdown virtualenv```
    >   En cualquier GNU/Linux Puede usar el gestor de paquetes de su preferencia, y es común que necesite ser usuario `Root`. De no tener acceso a `Root` para instalar, debe investigar sobre entornos virtuales con `virtualenv`.

1.  Ejecutar el programa:
    +   Estando en un entorno de escritorio, debe entrar en la carpeta de Bipylvo descargada, con doble clic en el script en la carpeta `bipylvo`:
        -   Windows: ```bipylvo\__main__.py```
            -   CMD: ```python bipylvo```
        -   GNU/Linux: ```bipylvo/__main__.py```
            -   SH: ```python3 bipylvo```

### Crear un ejecutable portátil:
Bipylvo esta programado de tal forma que pueda crearse un ejecutable portátil, empaquetado de manera que pueda ser usado en el mismo sistema operativo en el que fue compilado (creado), pero sin depender que el sistema anfitrión tenga todos las dependencias instaladas, que fueron definidas anteriormente. Solo el sistema que crea la compilación necesita tener instalado estos componentes.

Se recomienda usar un entorno virtual de python limpio para crear el ejecutable con solo los componentes necesarios, minimizando su peso. Los pasos para crear dicho ejecutable (SIN incluir la instalación del simulador) son

1.  Ejecutar una consola en su sistema operativo:
    -   Windows: ir al directorio que contiene el proyecto descargado, presionar clic derecho y seleccionar `Git Bash here`
    -   GNU/Linux: Abrir terminal en el menú de accesorios y/o utilidades.
        -   Ubicarse en la carpeta que contiene el sistema con el comando: ```cd ~/Bipylvo```

1.  Ejecutar el comando **PyInstaller**: ```pyinstaller Bipylvo.spec --clear```

1.  Probar el ejecutable creado en la carpeta `dist` haciendo doble clic en el archivo:
    -   Windows: `Bipylvo.exe`
    -   GNU/Linux: `Bipylvo`

1.  Copiar y redistribuir el archivo a otras computadoras con el mismo sistema operativo usado en su maquina anfitrión:
    >   Se diferencia versiones y bits del procesador, por ejemplo: `Windows 7 32bits` es distinto a `Windows 10 64bits`.
    >
    >   Es recomendable usar un sistema 32bits para realizar esto, ya que un compilado en 32bits funciona en un procesador y sistema operativo de 64bits, pero no de forma inversa.

### Binarios pre-compilados:
Se tienen creados dos (`2`) binarios usando el método anteriormente descrito. Actualmente los sistemas soportados son:
-   **[Ubuntu 17 (64bits)](https://sourceforge.net/projects/bipylvo/files/UbuntuBins/BipylvoLinux64bits/download)**
    >   Técnicamente puede funcionar en cualquier GNU/Linux amd64 (64bits) pero no ha sido probado a fondo, por eso se especificó que fue compilado en un Ubuntu 17.04.

-   **[Windows 10 (32bits)](https://sourceforge.net/projects/bipylvo/files/WindowsBins/BipylvoWindows32bits.exe/download)**
    >   El archivo fue creado en un sistema Windows 10 i386 (32bits), por lo que es funcional en Windows 10 32bits y Windows 10 64bits (Ambos probados).

>   Los binarios listados anteriormente no requieren instalaciones de *Python3* en el sistema operativo o de algunas de las dependencias descritas anteriormente. Pero si quiere actualizar el sistema debe descargar otro binario de la misma fuente.

## Uso del simulador:

Para usar el simulador es necesario una observar que el programa se divide en tres (`3`) pestañas:

1.  `Escenario`: contiene todos los parámetros necesarios para realizar una simulación, es la pestaña inicial del simulador permite definir los valores iniciales de una simulación y posteriormente ejecutar una o mas instancias del bivalvo seleccionado en los parámetros del escenario. Para ejecutar una simulación el usuario (usted) debe definir los siguientes valores:

    1.  `Especie a Simular`: es un selector desplegable que contiene la lista de bivalvos que pueden ser simulados en este sistema. Es necesario definir el bivalvo a simular para poder asignar temperaturas, cantidades de alimento, y para ejecutar simulaciones.

    2.  `Grupo Etario`: es un selector desplegable que contiene los grupos etarios que poseé la especie seleccionada para poder simular a un bivalvo que comprenda las características designadas para cada uno de estos grupos. Debe elegirse una especie a simular para que el sistema cargue la lista de grupos etarios disponibles.

    3.  `Alimento (%)`: es la cantidad de alimento que se le asigna diariamente a un bivalvo para su consumo. El alimento es un numero real positivo, mayor a cero (`0`), y representa el porcentaje del peso somático del animal que debe ser asignado como alimento diariamente.

    4.  `Tiempo (Días)`: es un numero entero que determina la cantidad de días que deben ser simulados. Se debe resaltar que cada animal a simular va a cumplir un numero de ciclos o días de simulación igual a los que se coloquen en esta variable.

    5.  `Cantidad (Bivalvos)`: es un numero entero que permite asignar el numero de entidades aleatorias que van a ser ejecutadas. Cada entidad esta aislada de la otra (como si fuera un bivalvo por cada pecera).

    6.  `Temperaturas (°C) para la simulación`: Es una lista de temperaturas a usar durante la simulación, la lista permite al usuario determinar como fluctuará las temperaturas entre el comienzo y el final de la misma lista. Las variaciones se hacen de forma lineal. Para agregar o remover temperaturas a la lista debe usar los siguientes botones disponibles:
        >   Nota: Actualmente no se puede organizar o filtrar las temperaturas registradas.

        1.  `Agregar`: permite agregar una (`1`) o mas temperaturas para la simulación mediante el uso de una ventana desplegable que tiene un método de entrada del tipo numérico real positivo, con limites designada según la `Especie a Simular` seleccionada, por lo que debe seleccionar la especie antes de agregar temperaturas a la lista. Actualmente solo puede agregar una temperatura al final de la lista.

        2.  `Eliminar`: permite eliminar una (`1`) temperatura seleccionada en el listado de temperaturas. Solo puede eliminar una temperatura a la vez.

    7.  `Iniciar Simulación`: ejecuta una validación de los parámetros del escenario de simulación, y de estar dentro de los parámetros permitidos se ejecuta una simulación. La ejecución de una simulación mostrará una ventana de carga que permite cancelar la ejecución. Posteriormente redirige al usuario a la pestaña de `Simulación`.

2.  `Simulación`: la sección de simulación se divide en tres (`3`) secciones que permiten observar los resultados y/o parámetros de la simulación realizada. La tabla de resultados es el componente principal y de mayor tamaño asignado en la ventana, dicha tabla contiene los valores resultantes de la simulación determinados por la especie a simular. Al ejercer clic en una de las filas de la tabla el simulador permite al usuario acceder directamente a la pestaña de `Bivalvo Seleccionado` cargando los datos del bivalvo seleccionado.
    >   Nota: Siempre es posible saltar entre cualquiera de las pestañas existentes, incluso cuando no se tienen datos simulados.

3.  `Bivalvo Seleccionado`: esta es la sección de detalles, que permite observar con comodidad los valores de los atributos del bivalvo seleccionado, para un día de simulación especificado. La pestaña cuenta con los siguientes componentes:

    1.  Selección del bivalvo y día: la sección cuenta con dos (`2`) selectores que permiten buscar por Identificador (`ID`) y por `Día`, los cuales despliegan los cambios cuando sus valores son modificados.

    2.  La pestaña de `resultados`: contiene una lista con los variables y acumuladores del bivalvo seleccionado para el día de simulación especificado anteriormente, aquí puede observar con detalle los cambios de.

    3.  La pestaña de `eventos`: contiene los registros de los sucesos relevantes en la simulación del bivalvo seleccionado. El listado es completo y no se ve limitado según el `Día` seleccionado.

        >   Navegar entre estas pestañas no afecta a las pestañas principales o a los controles del selector de bivalvos.

    4.  El selector de gráficas: titulado `Gráficas Disponibles`, permite elegir entre las gráficas lineales que pueden generarse con este simulador. Una vez seleccionada una gráfica de la lista, el programa abre una ventana y creando la gráfica, con las opciones disponibles para su manejo.

#### Ejemplo de uso:
1.  Inicia el programa.

2.  Elije una especie de bivalvo y después prosigue a llenar el resto de los parámetros.

    | Campo             | Valores                               |
    |-------------------|---------------------------------------|
    | 1. Especie:       | `Euvola ziczac Tropical`              |
    | 2. Grupo Etario:  | `1) Juveniles`                        |
    | 3. Alimento:      | `6` %                                 |
    | 4. Tiempo:        | `30` días                             |
    | 5. Cantidad:      | `20` vieiras                          |
    | 6. Temperatura:   | Agregar `20` °C y salir del dialogo.  |

3.  Ejecuta la simulación.

4.  Observa los resultados.

5.  Elige un bivalvo y leé los detalles de interés.

6.  Navega entre los días de simulación observando los datos y sus cambios.

7.  Puede concluir el ejercicio.
